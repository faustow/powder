# Gnar Powder

[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)
[![codecov](https://codecov.io/gl/gnaar/powder/branch/master/graph/badge.svg?token=MRowdXaujg)](https://codecov.io/gl/gnaar/powder)
[![pipeline status](https://gitlab.com/gnaar/powder/badges/master/pipeline.svg)](https://gitlab.com/gnaar/powder/commits/master)

**Part of Project Gnar:** &nbsp;[base](https://hub.docker.com/r/gnar/base) &nbsp;•&nbsp; [gear](https://pypi.org/project/gnar-gear) &nbsp;•&nbsp; [piste](https://gitlab.com/gnaar/piste) &nbsp;•&nbsp; [off-piste](https://gitlab.com/gnaar/off-piste) &nbsp;•&nbsp; [edge](https://www.npmjs.com/package/gnar-edge) &nbsp;•&nbsp; [powder](https://gitlab.com/gnaar/powder)  &nbsp;•&nbsp; [patrol](https://gitlab.com/gnaar/patrol)


# W I P
