module.exports = {
  presets: [
    '@babel/preset-env',
    '@babel/react'
  ],
  env: {
    production: {
      presets: ['react-optimize']
    }
  },
  plugins: [
    'react-hot-loader/babel',
    '@babel/transform-runtime',
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-proposal-function-bind',
    '@babel/plugin-proposal-export-default-from',
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    ['module-resolver', {
      root: ['./src'],
      alias: {
        '~': './',
        actions: './src/js/redux/actions',
        api: './src/js/api',
        components: './src/js/components',
        errors: './src/js/errors',
        i18n: './src/js/i18n',
        mocks: './src/__mocks__',
        reducers: './src/js/redux/reducers',
        sagas: './src/js/redux/sagas',
        util: './src/js/util',
        views: './src/js/views'
      }
    }]
  ]
};
