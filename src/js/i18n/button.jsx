import { connect } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Popover from '@material-ui/core/Popover';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Select from '@material-ui/core/Select';

import i18n, { languages, parseJson } from 'i18n';
import { languageActions } from 'actions';
import languageIcon from 'assets/images/languageIcon.png';
import withStyles from '@material-ui/core/styles/withStyles';

import './button.scss';

const styles = theme => ({
  image: {
    opacity: .2  // opacity added in the theme class because otherwise the transition is applied on render
  },
  popoverTypography: {
    margin: theme.spacing.unit * 2
  }
});

const mapDispatchToProps = languageActions;

const mapStateToProps = state => ({
  language: state.language.get('current')
});

@withStyles(styles)
@connect(mapStateToProps, mapDispatchToProps)
export default class I18nButton extends Component {
  static propTypes = {
    classes: PropTypes.shape({
      popoverTypography: PropTypes.string.isRequired
    }).isRequired,
    language: PropTypes.string.isRequired,
    setLanguage: PropTypes.func.isRequired
  }

  state = {
    popoverOpen: false
  }

  setLanguage = nextLanguage => {
    const { setLanguage } = this.props;
    this.setState({ popoverOpen: false });
    if (nextLanguage in i18n) {
      setLanguage(nextLanguage);
    } else {
      import(/* webpackChunkName: "assets/i18n/[request]" */ `./${nextLanguage.toLowerCase()}.json`).then(json => {
        i18n[nextLanguage] = parseJson(json);
        setLanguage(nextLanguage);
      });
    }
  }

  togglePopup = () => {
    this.setState(({ popoverOpen }) => ({ popoverOpen: !popoverOpen }));
  }

  render() {
    const { classes, language } = this.props;
    const { popoverOpen } = this.state;
    return (
      <div className='i18n-button'>
        <IconButton
          buttonRef={node => { this.anchorEl = node; }}
          onClick={this.togglePopup}
        >
          <img
            alt='Change Language'
            className={classes.image}
            src={languageIcon}
          />
        </IconButton>
        <Popover
          anchorEl={this.anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          open={popoverOpen}
          transformOrigin={{
            vertical: 'bottom',
            horizontal: 'left'
          }}
          onClose={this.togglePopup}
        >
          <Select
            className={classes.popoverTypography}
            value={language}
            onChange={({ target: { value } }) => { this.setLanguage(value); }}
          >
            {languages.map(({ text, value }) => <MenuItem key={value} value={value}>{text}</MenuItem>)}
          </Select>
        </Popover>
      </div>
    );
  }
}
