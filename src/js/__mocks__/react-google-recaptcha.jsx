import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

import { reCaptchaResponse } from 'mocks/constants';

export default class ReCAPTCHA extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired
  }

  reset = () => {}

  execute() { this.props.onChange(reCaptchaResponse); }

  render() {
    return <div ref={() => {}} />;
  }
}
