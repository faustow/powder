Place mocks of node modules in this directory.

Jest will [automatically mock node modules](https://jestjs.io/docs/en/manual-mocks) using the mocks in this directory.
