import { Notifications } from 'gnar-edge/es/notifications';
import { isLoggedIn } from 'gnar-edge/es/jwt';
import Grid from '@material-ui/core/Grid';
import React from 'react';
import Redirect from 'react-router-dom/Redirect';
import Route from 'react-router-dom/Route';
import Switch from 'react-router-dom/Switch';

import AccountDetailsRouteHandler from 'views/accountDetails';
import ActivateRouteHandler from 'views/activate';
import AuthenticatedRoute from './authenticatedRoute';
import Header from 'components/header';
import LoginRouteHandler from 'views/login';
import SendResetPasswordEmailRouteHandler from 'views/sendResetPasswordEmail';
import SetPasswordRouteHandler from 'views/setPassword';
import SignupRouteHandler from 'views/signup';

import 'animate.css/source/_base.css';
import './routes.scss';

export default (
  <Grid container alignItems='flex-start' className='route'>
    <Grid item align='center' className='logo' xs={12}>
      <Header />
    </Grid>
    <Grid item className='content' xs={12}>
      <Grid container>
        <Grid item xs={12}>
          <Switch>
            <AuthenticatedRoute component={AccountDetailsRouteHandler} path='/account' />
            <Route component={ActivateRouteHandler} path='/activate/:token' />
            <Route component={LoginRouteHandler} path='/login' />
            <Route component={SendResetPasswordEmailRouteHandler} path='/send-reset-password-email' />
            <Route component={SetPasswordRouteHandler} path='/set-password' />
            <Route component={SignupRouteHandler} path='/signup' />
            <Route component={SetPasswordRouteHandler} path='/reset-password/:token' />
            <Route render={() => <Redirect to={isLoggedIn() ? '/account' : '/login'} />} />
          </Switch>
          <Notifications />
        </Grid>
      </Grid>
    </Grid>
  </Grid>
);
