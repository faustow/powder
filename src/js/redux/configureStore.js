import { applyMiddleware, compose, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './reducers';
import sagas from './sagas';

const isProd = process.env.NODE_ENV === 'production';

export default history => {
  const sagaMiddleware = createSagaMiddleware();

  const composer = isProd ? compose : composeWithDevTools;

  const store = createStore(rootReducer, composer(applyMiddleware(sagaMiddleware)));

  sagaMiddleware.run(sagas, { history });

  /* istanbul ignore if */
  if (module.hot) {
    module.hot.accept('./reducers', () => {
      store.replaceReducer(rootReducer);
    });
  }

  return store;
};
