import { Map } from 'immutable';
import { handleActions } from 'redux-actions';

export const ACCOUNT_ACTIVATED = 'ACCOUNT_ACTIVATED';
export const ACCOUNT_ACTIVATED_ERROR = 'ACCOUNT_ACTIVATED_ERROR';

const reducers = {
  [ACCOUNT_ACTIVATED]: (state, { payload }) => state.merge(payload),
  [ACCOUNT_ACTIVATED_ERROR]: (state, { payload }) => state.merge(payload)
};

const initialState = () => Map();

export default handleActions(reducers, initialState());
