# Redux Reducers

Project Gnar Tips:
- Keep your Redux Reducer files small
- Choose meaningful names
- Using consistent naming across actions, reducers, and sagas
- Register each new reducer file in the `index.js` file in this folder
