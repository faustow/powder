import { Map } from 'immutable';
import { handleActions } from 'redux-actions';

export const LOGIN_ERROR = 'LOGIN_ERROR';

const reducers = {
  [LOGIN_ERROR]: (state, { payload }) => state.merge(payload)
};

const initialState = () => Map();

export default handleActions(reducers, initialState());
