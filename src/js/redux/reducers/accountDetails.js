import { Map } from 'immutable';
import { handleActions } from 'redux-actions';

export const ACCOUNT_DETAILS = 'ACCOUNT_DETAILS';

const reducers = {
  [ACCOUNT_DETAILS]: (state, { payload }) => state.clear().merge(payload)
};

const initialState = () => Map();

export default handleActions(reducers, initialState());
