import { Map } from 'immutable';
import { handleActions } from 'redux-actions';

export const SET_PWNED_PASSWORD_COUNT = 'SET_PWNED_PASSWORD_COUNT';

const reducers = {
  [SET_PWNED_PASSWORD_COUNT]: (state, { payload }) =>
    (payload.passwordScoreDispatchedAt > state.get('passwordScoreDispatchedAt', 0) ? state.merge(payload) : state)
};

const initialState = () => Map();

export default handleActions(reducers, initialState());
