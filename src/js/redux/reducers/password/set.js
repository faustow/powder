import { Map } from 'immutable';
import { handleActions } from 'redux-actions';

export const SET_PASSWORD_ERROR = 'SET_PASSWORD_ERROR';

const reducers = {
  [SET_PASSWORD_ERROR]: (state, { payload }) => state.merge(payload)
};

const initialState = () => Map();

export default handleActions(reducers, initialState());
