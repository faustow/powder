import { combineReducers } from 'redux';
import { notifications } from 'gnar-edge/es/notifications';
import { routerReducer as routing } from 'react-router-redux';

import accountDetails from './accountDetails';
import activate from './activate';
import language from './language';
import login from './login';
import pwnedPasswordCount from './password/countPwned';
import passwordScore from './password/score';
import setPassword from './password/set';

// [Project Gnar]: The default export defines your store, the import names define the keys of the store.

export default combineReducers({
  accountDetails,
  activate,
  language,
  login,
  notifications,
  passwordScore,
  pwnedPasswordCount,
  routing,
  setPassword
});
