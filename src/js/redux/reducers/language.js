import { Map } from 'immutable';
import { handleActions } from 'redux-actions';

import { ENGLISH } from 'i18n';
import { SET_LANGUAGE } from 'actions/language';

const reducers = {
  [SET_LANGUAGE]: (state, { payload }) => state.merge(payload)
};

const initialState = () => Map({ current: ENGLISH });

export default handleActions(reducers, initialState());
