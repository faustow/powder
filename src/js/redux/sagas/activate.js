import base64 from 'gnar-edge/es/base64';
import { delay } from 'redux-saga';
import { handleApiError } from 'util';

import { call, put, takeEvery } from 'redux-saga/effects';
import { ACCOUNT_ACTIVATED, ACCOUNT_ACTIVATED_ERROR } from 'reducers/activate';
import { SUBMIT_ACTIVATION } from 'actions/activate';
import { userApi } from 'api';


function* activate({ history }, { payload: { token } }) {
  try {
    const response = yield call(userApi.activate, token);
    yield delay(2000);
    if (response.error) {
      yield call(handleApiError, ACCOUNT_ACTIVATED_ERROR, response.error);
    } else {
      const [email] = base64.decode(token).split('/');
      const { userId, accessToken } = response;
      localStorage.setItem('jwt', accessToken);
      yield put({ type: ACCOUNT_ACTIVATED, payload: { email, userId } });
      yield call(history.replace, '/set-password');
    }
  } catch ({ message }) {
    yield call(handleApiError, ACCOUNT_ACTIVATED_ERROR, message);
  }
}

export default function* watchActivate(context) {
  yield takeEvery(SUBMIT_ACTIVATION, activate, context);
}
