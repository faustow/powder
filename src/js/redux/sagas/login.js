import { handleApiError } from 'util';

import { call, put, takeEvery } from 'redux-saga/effects';
import { ACCOUNT_DETAILS } from 'reducers/accountDetails';
import { LOGIN_ERROR } from 'reducers/login';
import { SUBMIT_LOGIN } from 'actions/login';
import { userApi } from 'api';

function* submitLogin({ history }, { payload: { email, password, reCaptchaResponse } }) {
  try {
    const response = yield call(userApi.login, email, password, reCaptchaResponse);
    if (response.error) {
      yield call(handleApiError, LOGIN_ERROR, response.error, { reset: true });
    } else {
      yield put({ type: ACCOUNT_DETAILS, payload: response });
      yield call(history.replace, '/account');
    }
  } catch ({ message }) {
    yield call(handleApiError, LOGIN_ERROR, message, { reset: true });
  }
}

export default function* watchSubmitLogin(context) {
  yield takeEvery(SUBMIT_LOGIN, submitLogin, context);
}
