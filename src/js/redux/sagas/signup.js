import { call, takeEvery } from 'redux-saga/effects';
import { SUBMIT_SIGNUP_EMAIL } from 'actions/signup';
import { userApi } from 'api';

function* submitSignupEmail({ history }, { payload: { email, reCaptchaResponse } }) {
  try {
    yield call(userApi.signup, email, reCaptchaResponse);
    history.push('login');
  } catch (e) {
    // Handled by the Api
  }
}

export default function* watchSubmitSignupEmail(context) {
  yield takeEvery(SUBMIT_SIGNUP_EMAIL, submitSignupEmail, context);
}
