import { call, takeEvery } from 'redux-saga/effects';
import { SEND_RESET_PASSWORD_EMAIL } from 'actions/sendResetPasswordEmail';
import { userApi } from 'api';

function* sendResetPasswordEmail({ payload: { email, reCaptchaResponse } }) {
  try {
    yield call(userApi.sendResetPasswordEmail, email, reCaptchaResponse);
  } catch (e) {
    // Handled by the Api
  }
}

export default function* watchSendResetPasswordEmail() {
  yield takeEvery(SEND_RESET_PASSWORD_EMAIL, sendResetPasswordEmail);
}
