import { all, fork } from 'redux-saga/effects';
import { watchGetAccountDetails, watchSetAccountDetails } from './accountDetails';
import watchActivate from './activate';
import watchCountPwnedPassword from './password/countPwned';
import watchResetPassword from './password/reset';
import watchScorePassword from './password/score';
import watchSendResetPasswordEmail from './sendResetPasswordEmail';
import watchSetPassword from './password/set';
import watchSubmitLogin from './login';
import watchSubmitLogout from './logout';
import watchSubmitSignupEmail from './signup';

export default function* (context = {}) {
  yield all([
    watchActivate,
    watchCountPwnedPassword,
    watchGetAccountDetails,
    watchResetPassword,
    watchScorePassword,
    watchSendResetPasswordEmail,
    watchSetAccountDetails,
    watchSetPassword,
    watchSubmitLogin,
    watchSubmitLogout,
    watchSubmitSignupEmail
  ].map(saga => fork(saga, context)));
}
