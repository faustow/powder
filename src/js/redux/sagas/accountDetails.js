import { call, put, takeEvery } from 'redux-saga/effects';
import { ACCOUNT_DETAILS } from 'reducers/accountDetails';
import { GET_ACCOUNT_DETAILS, SET_ACCOUNT_DETAILS } from 'actions/accountDetails';
import { userApi } from 'api';

function* getAccountDetails(context, { payload: { email } }) {
  try {
    const response = yield call(userApi.get, email);
    yield put({ type: ACCOUNT_DETAILS, payload: response });
  } catch (e) {
    // Handled by the Api
  }
}

export function* watchGetAccountDetails(context) {
  yield takeEvery(GET_ACCOUNT_DETAILS, getAccountDetails, context);
}

function* setAccountDetails(context, { payload: { userId, data } }) {
  if (!userId) {
    return;
  }
  try {
    yield call(userApi.update, userId, data);
  } catch (error) {
    // Handled by the Api
  }
}

export function* watchSetAccountDetails(context) {
  yield takeEvery(SET_ACCOUNT_DETAILS, setAccountDetails, context);
}
