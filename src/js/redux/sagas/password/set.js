import { delay } from 'redux-saga';

import { call, put, takeEvery } from 'redux-saga/effects';
import { SET_PASSWORD } from 'actions/password/set';
import { SET_PASSWORD_ERROR } from 'reducers/password/set';
import { userApi } from 'api';

function* setPassword({ history }, { payload: { userId, password } }) {
  try {
    const response = yield call(userApi.update, userId, { password });
    if (response.error) {
      yield put({ type: SET_PASSWORD_ERROR, payload: { error: response.error } });
      yield delay(2000);
      yield put({ type: SET_PASSWORD_ERROR, payload: { error: null } });
    } else {
      yield call(history.replace, '/account');
    }
  } catch (e) {
    // Handled by the Api
  }
}

export default function* watchSetPassword(context) {
  yield takeEvery(SET_PASSWORD, setPassword, context);
}
