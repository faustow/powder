import { handleApiError } from 'util';
import { notifySuccess } from 'gnar-edge/es/notifications';

import { call, takeEvery } from 'redux-saga/effects';
import { RESET_PASSWORD } from 'actions/password/reset';
import { SET_PASSWORD_ERROR } from 'reducers/password/set';
import { userApi } from 'api';

function* resetPassword({ history }, { payload: { actionId, email, password } }) {
  try {
    const response = yield call(userApi.resetPassword, actionId, email, password);
    if (response.error) {
      yield call(handleApiError, SET_PASSWORD_ERROR, response.error, { reset: true });
    } else {
      yield notifySuccess('Password changed! Please login with your new password.');
      yield call(history.replace, '/login');
    }
  } catch ({ message }) {
    yield call(handleApiError, SET_PASSWORD_ERROR, message, { reset: true });
  }
}

export default function* watchResetPassword(context) {
  yield takeEvery(RESET_PASSWORD, resetPassword, context);
}
