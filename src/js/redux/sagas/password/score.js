import { call, put, takeEvery } from 'redux-saga/effects';
import { GET_PASSWORD_SCORE } from 'actions/password/score';
import { SET_PASSWORD_SCORE } from 'reducers/password/score';
import { GET_PWNED_PASSWORD_COUNT } from 'sagas/password/countPwned';
import { userApi } from 'api';

function* scorePassword({ payload: { password, passwordScoreDispatchedAt } }) {
  try {
    const result = yield call(userApi.scorePassword, password);
    yield put({ type: SET_PASSWORD_SCORE, payload: { ...result, passwordScoreDispatchedAt } });
    if (result.score > 3) {
      yield put({ type: GET_PWNED_PASSWORD_COUNT, payload: { password, passwordScoreDispatchedAt } });
    }
  } catch (e) {
    // Handled by the Api
  }
}

export default function* watchScorePassword() {
  yield takeEvery(GET_PASSWORD_SCORE, scorePassword);
}
