import { put, takeEvery } from 'redux-saga/effects';
import { SET_PWNED_PASSWORD_COUNT } from 'reducers/password/countPwned';

export const GET_PWNED_PASSWORD_COUNT = 'GET_PWNED_PASSWORD_COUNT';

function* getPwnedPasswordCount({ payload: { password, passwordScoreDispatchedAt } }) {
  const digest = yield crypto.subtle.digest('SHA-1', new TextEncoder('UTF-8').encode(password))
    .then(data => Array.from(new Uint8Array(data)).map(b => b.toString(16).padStart(2, '0')).join(''));
  const matches = yield fetch(`https://api.pwnedpasswords.com/range/${digest.substr(0, 5)}`).then(r => r.text());
  const match = matches.match(RegExp(`${digest.substr(5)}:(\\d+)`, 'i'));
  const count = match ? parseInt(match[1], 10) : 0;
  yield put({ type: SET_PWNED_PASSWORD_COUNT, payload: { count, passwordScoreDispatchedAt } });
}

export default function* watchCountPwnedPassword() {
  yield takeEvery(GET_PWNED_PASSWORD_COUNT, getPwnedPasswordCount);
}
