import { call, put, takeEvery } from 'redux-saga/effects';
import { ACCOUNT_DETAILS } from 'reducers/accountDetails';
import { SUBMIT_LOGOUT } from 'actions/logout';

function* submitLogout({ history }) {
  localStorage.clear();
  yield put({ type: ACCOUNT_DETAILS, payload: {} });
  yield call(history.replace, '/login');
}

export default function* watchSubmitLogout(context) {
  yield takeEvery(SUBMIT_LOGOUT, submitLogout, context);
}
