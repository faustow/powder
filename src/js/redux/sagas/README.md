# Redux Sagas

Project Gnar Tips:
- Keep your Redux Saga files small
- Choose meaningful names
- Using consistent naming across actions, reducers, and sagas
- Register each new saga file in the `index.js` file in this folder
