# Redux Actions

Project Gnar Tips:
- Keep your Redux Action files small
- Choose meaningful names
- Using consistent naming across actions, reducers, and sagas
- Register each new action file in the `index.js` file in this folder
