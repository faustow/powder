import { createAction } from 'redux-actions';

export const SUBMIT_SIGNUP_EMAIL = 'SUBMIT_SIGNUP_EMAIL';

export default {
  submitSignupEmail: createAction(SUBMIT_SIGNUP_EMAIL, (email, reCaptchaResponse) => ({ email, reCaptchaResponse }))
};
