import { createAction } from 'redux-actions';

export const SET_LANGUAGE = 'SET_LANGUAGE';

export default {
  setLanguage: createAction(SET_LANGUAGE, language => ({ current: language }))
};
