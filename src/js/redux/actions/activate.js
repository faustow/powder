import { createAction } from 'redux-actions';

export const SUBMIT_ACTIVATION = 'SUBMIT_ACTIVATION';

export default {
  submitActivation: createAction(SUBMIT_ACTIVATION, token => ({ token }))
};
