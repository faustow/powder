import { createAction } from 'redux-actions';

export const SEND_RESET_PASSWORD_EMAIL = 'SEND_RESET_PASSWORD_EMAIL';

export default {
  sendResetPasswordEmail: createAction(SEND_RESET_PASSWORD_EMAIL, (email, reCaptchaResponse) =>
    ({ email, reCaptchaResponse }))
};
