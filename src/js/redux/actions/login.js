import { createAction } from 'redux-actions';

export const SUBMIT_LOGIN = 'SUBMIT_LOGIN';

export default {
  submitLogin: createAction(SUBMIT_LOGIN, (email, password, reCaptchaResponse) =>
    ({ email, password, reCaptchaResponse }))
};
