import { createAction } from 'redux-actions';

export const SUBMIT_LOGOUT = 'SUBMIT_LOGOUT';

export default {
  submitLogout: createAction(SUBMIT_LOGOUT, () => ({}))
};
