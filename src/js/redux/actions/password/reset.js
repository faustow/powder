import { createAction } from 'redux-actions';

export const RESET_PASSWORD = 'RESET_PASSWORD';

export default {
  resetPassword: createAction(RESET_PASSWORD, (actionId, email, password) => ({ actionId, email, password }))
};
