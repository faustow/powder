import { createAction } from 'redux-actions';

export const SET_PASSWORD = 'SET_PASSWORD';

export default {
  setPassword: createAction(SET_PASSWORD, (userId, password) => ({ userId, password }))
};
