import { createAction } from 'redux-actions';

export const GET_PASSWORD_SCORE = 'GET_PASSWORD_SCORE';

export default {
  getPasswordScore: createAction(GET_PASSWORD_SCORE,
    (password, passwordScoreDispatchedAt) => ({ password, passwordScoreDispatchedAt }))
};
