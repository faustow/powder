import { createAction } from 'redux-actions';

export const GET_ACCOUNT_DETAILS = 'GET_ACCOUNT_DETAILS';
export const SET_ACCOUNT_DETAILS = 'SET_ACCOUNT_DETAILS';

export default {
  getAccountDetails: createAction(GET_ACCOUNT_DETAILS, email => ({ email })),
  setAccountDetails: createAction(SET_ACCOUNT_DETAILS, (userId, data) => ({ userId, data }))
};
