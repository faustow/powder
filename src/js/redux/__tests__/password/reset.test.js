import { delay } from 'redux-saga';
import { drain } from 'gnar-edge';

import { ConsoleMock, FetchMock, LocalStorageMock, requestFailure } from 'mocks';
import { actionId, email, password } from 'mocks/constants';
import configureStore from 'js/redux/configureStore';
import resetPasswordActions from 'actions/password/reset';

describe('redux resetPassword', () => {
  it('submits the correct request and redirects to login on successful resetPassword', drain(function* () {
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const fetchMock = new FetchMock();
    yield store.dispatch(resetPasswordActions.resetPassword(actionId, email, password));
    const body = JSON.stringify({ actionId, email, password });
    expect(fetchMock.fn).toHaveBeenCalledWith('/user/reset-password', { method: 'POST', body });
    fetchMock.deactivate();
  }));

  it('propagates an error to the store when resetPassword fails', drain(function* () {
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const error = 'Password failed validation on server.';
    const consoleMock = new ConsoleMock();
    const fetchMock = new FetchMock({ error });
    const localStorageMock = new LocalStorageMock();
    yield store.dispatch(resetPasswordActions.resetPassword(actionId, email, password));
    expect(consoleMock.error).toHaveBeenCalledWith(error);
    expect(store.getState().setPassword.toJSON()).toEqual({ error });
    expect(localStorage.setItem).not.toHaveBeenCalledWith();
    yield delay(2000);
    expect(store.getState().setPassword.toJSON()).toEqual({ error: null });
    consoleMock.deactivate();
    fetchMock.deactivate();
    localStorageMock.deactivate();
  }));

  it('propagates an error to the store when the request fails', drain(function* () {
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const consoleMock = new ConsoleMock();
    const fetchMock = new FetchMock({}, { fail: true });
    const localStorageMock = new LocalStorageMock();
    yield store.dispatch(resetPasswordActions.resetPassword(actionId, email, password));
    expect(consoleMock.error).toHaveBeenCalledWith(requestFailure);
    expect(store.getState().setPassword.toJSON()).toEqual({ error: requestFailure.message });
    expect(localStorage.setItem).not.toHaveBeenCalledWith();
    yield delay(2000);
    expect(store.getState().setPassword.toJSON()).toEqual({ error: null });
    consoleMock.deactivate();
    fetchMock.deactivate();
    localStorageMock.deactivate();
  }));
});
