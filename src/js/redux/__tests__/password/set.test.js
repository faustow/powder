import { delay } from 'redux-saga';
import { drain } from 'gnar-edge';

import { ConsoleMock, FetchMock, LocalStorageMock, withAuthRequestHeader } from 'mocks';
import { userId, jwt, password } from 'mocks/constants';
import configureStore from 'js/redux/configureStore';
import setPasswordActions from 'actions/password/set';

const notificationsSagas = require('gnar-edge/es/notifications/redux/sagas');

notificationsSagas.notifyError = jest.fn();

describe('redux setPassword', () => {
  it('submits the correct request and redirects to account on successful setPassword', drain(function* () {
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const fetchMock = new FetchMock({}, { withAuthResponseHeader: true });
    const localStorageMock = new LocalStorageMock();
    localStorage.setItem('jwt', jwt);
    yield store.dispatch(setPasswordActions.setPassword(userId, password));
    const body = JSON.stringify({ password });
    expect(fetchMock.fn)
      .toHaveBeenCalledWith(`/user/${userId}`, withAuthRequestHeader({ method: 'PUT', body }));
    expect(historyMock.replace).toHaveBeenCalledWith('/account');
    fetchMock.deactivate();
    localStorageMock.deactivate();
  }));

  it('propagates an error to the store when setPassword fails', drain(function* () {
    const consoleMock = new ConsoleMock();
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const error = 'Password failed validation on server.';
    const fetchMock = new FetchMock({ error }, { withAuthResponseHeader: true });
    const localStorageMock = new LocalStorageMock();
    localStorage.setItem('jwt', jwt);
    yield store.dispatch(setPasswordActions.setPassword(userId, password));
    const body = JSON.stringify({ password });
    expect(fetchMock.fn)
      .toHaveBeenCalledWith(`/user/${userId}`, withAuthRequestHeader({ method: 'PUT', body }));
    expect(consoleMock.error).toHaveBeenCalledWith(error);
    expect(notificationsSagas.notifyError).toHaveBeenCalledWith(error);
    expect(store.getState().setPassword.get('error')).toBe(error);
    yield delay(2000);
    expect(store.getState().setPassword.get('error')).toBe(null);
    expect(historyMock.replace).not.toHaveBeenCalled();
    consoleMock.deactivate();
    fetchMock.deactivate();
    localStorageMock.deactivate();
  }));
});
