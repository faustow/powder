import { delay } from 'redux-saga';
import { drain } from 'gnar-edge';
import Chance from 'chance';

import { FetchMock } from 'mocks';
import { password } from 'mocks/constants';
import configureStore from 'js/redux/configureStore';
import scorePasswordActions from 'actions/password/score';

const chance = new Chance();
const notificationsSagas = require('gnar-edge/es/notifications/redux/sagas');

notificationsSagas.notifyError = jest.fn();

const getDigest = pwd => crypto.subtle.digest('SHA-1', new TextEncoder('UTF-8').encode(pwd))
  .then(data => Array.from(new Uint8Array(data)).map(b => b.toString(16).padStart(2, '0')).join(''));

describe('redux setPassword', () => {
  it('submits the correct request and properly updates the store on scorePassword', drain(function* () {
    const store = configureStore();
    const passwordScoreDispatchedAt = new Date().getTime();
    const payload = { score: 0, feedback: { warning: 'Too weak' } };
    const fetchMock = new FetchMock(payload);
    yield store.dispatch(scorePasswordActions.getPasswordScore(password, passwordScoreDispatchedAt));
    const body = JSON.stringify({ password });
    expect(fetchMock.fn).toHaveBeenCalledWith('/user/score-password', { method: 'POST', body });
    expect(store.getState().passwordScore.toJSON()).toEqual({ passwordScoreDispatchedAt, ...payload });
    fetchMock.deactivate();
  }));

  it(
    'properly updates the store when the password score is 4 and there are matches in the pwnedpasswords response',
    drain(function* () {
      const store = configureStore();
      const passwordScoreDispatchedAt = new Date().getTime();
      const payload = { score: 4 };
      const digest = yield getDigest(password);
      const fetchMock = new FetchMock(url => ({
        '/user/score-password': payload,
        [`https://api.pwnedpasswords.com/range/${digest.substr(0, 5)}`]: `${digest}:42`
      })[url]);
      yield store.dispatch(scorePasswordActions.getPasswordScore(password, passwordScoreDispatchedAt));
      const body = JSON.stringify({ password });
      expect(fetchMock.fn).toHaveBeenCalledWith('/user/score-password', { method: 'POST', body });
      expect(store.getState().passwordScore.toJSON()).toEqual({ passwordScoreDispatchedAt, ...payload });
      expect(fetchMock.fn).toHaveBeenCalledWith(`https://api.pwnedpasswords.com/range/${digest.substr(0, 5)}`);
      yield setTimeout(() => {
        expect(store.getState().pwnedPasswordCount.toJSON()).toEqual({ passwordScoreDispatchedAt, count: 42 });
      });
      fetchMock.deactivate();
    })
  );

  it(
    'properly updates the store when the password score is 4 and there are no matches in the pwnedpasswords response',
    drain(function* () {
      const store = configureStore();
      const passwordScoreDispatchedAt = new Date().getTime();
      const payload = { score: 4 };
      const digest = yield getDigest(password);
      const mockPwnedPasswordsResponse = _.range(chance.integer({ max: 584, min: 381 }))
        .map(() => `${chance.hash({ length: 35 })}:${chance.integer({ max: 100000, min: 1 })}`).join('\n');
      const fetchMock = new FetchMock(url => ({
        '/user/score-password': payload,
        [`https://api.pwnedpasswords.com/range/${digest.substr(0, 5)}`]: mockPwnedPasswordsResponse
      })[url]);
      yield store.dispatch(scorePasswordActions.getPasswordScore(password, passwordScoreDispatchedAt));
      const body = JSON.stringify({ password });
      expect(fetchMock.fn).toHaveBeenCalledWith('/user/score-password', { method: 'POST', body });
      expect(store.getState().passwordScore.toJSON()).toEqual({ passwordScoreDispatchedAt, ...payload });
      expect(fetchMock.fn).toHaveBeenCalledWith(`https://api.pwnedpasswords.com/range/${digest.substr(0, 5)}`);
      yield setTimeout(() => {
        expect(store.getState().pwnedPasswordCount.toJSON()).toEqual({ passwordScoreDispatchedAt, count: 0 });
      });
      fetchMock.deactivate();
    })
  );

  it(
    'does not override the results of a newer password score with that of an older password score',
    drain(function* () {
      const store = configureStore();
      const payload = { score: 0, feedback: {} };
      let state = 0;
      const fetchMock = new FetchMock(() => (state++ === 0
        ? new Promise(resolve => { setTimeout(() => { resolve(payload); }, 100); })
        : payload
      ));
      const password1 = 'Password1';
      const passwordScoreDispatchedAt1 = new Date().getTime();
      yield store.dispatch(scorePasswordActions.getPasswordScore(password1, passwordScoreDispatchedAt1));
      yield delay(10);
      const password2 = 'Password12';
      const passwordScoreDispatchedAt2 = new Date().getTime();
      yield store.dispatch(scorePasswordActions.getPasswordScore(password2, passwordScoreDispatchedAt2));
      const body1 = JSON.stringify({ password: password1 });
      const body2 = JSON.stringify({ password: password2 });
      expect(fetchMock.fn).toHaveBeenCalledWith('/user/score-password', { method: 'POST', body: body1 });
      expect(fetchMock.fn).toHaveBeenCalledWith('/user/score-password', { method: 'POST', body: body2 });
      yield delay(200);
      const passwordScore = store.getState().passwordScore.toJSON();
      expect(passwordScore).toEqual({ passwordScoreDispatchedAt: passwordScoreDispatchedAt2, ...payload });
      fetchMock.deactivate();
    })
  );

  it(
    'does not override the results of a newer pwnedPasswordCount with that of an older pwnedPasswordCount',
    drain(function* () {
      const store = configureStore();
      const payload = { score: 4 };
      let state = 0;
      const fetchMock = new FetchMock(url => (url === '/user/score-password'
        ? state++ === 0
          ? new Promise(resolve => { setTimeout(() => { resolve(payload); }, 100); })
          : payload
        : ''
      ));
      const password1 = 'Password1';
      const passwordScoreDispatchedAt1 = new Date().getTime();
      yield store.dispatch(scorePasswordActions.getPasswordScore(password1, passwordScoreDispatchedAt1));
      yield delay(10);
      const password2 = 'Password12';
      const passwordScoreDispatchedAt2 = new Date().getTime();
      yield store.dispatch(scorePasswordActions.getPasswordScore(password2, passwordScoreDispatchedAt2));
      yield delay(200);
      const pwnedPasswordCount = store.getState().pwnedPasswordCount.toJSON();
      expect(pwnedPasswordCount).toEqual({ passwordScoreDispatchedAt: passwordScoreDispatchedAt2, count: 0 });
      fetchMock.deactivate();
    })
  );
});
