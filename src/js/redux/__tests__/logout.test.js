import { drain } from 'gnar-edge';

import { LocalStorageMock } from 'mocks';
import configureStore from 'js/redux/configureStore';
import logoutActions from 'actions/logout';

describe('redux logout', () => {
  it('clears localStorage and redirects to login on logout', drain(function* () {
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const localStorageMock = new LocalStorageMock();
    yield store.dispatch(logoutActions.submitLogout());
    expect(localStorage.clear).toHaveBeenCalled();
    expect(historyMock.replace).toHaveBeenCalledWith('/login');
    localStorageMock.deactivate();
  }));
});
