import sagas from 'js/redux/sagas';

describe('redux sagas', () => {
  it('properly updates the store and redirects to account on successful login', () => {
    const gen = sagas();
    const first = gen.next();
    const last = gen.next();
    expect(first.done).toBe(false);
    expect(_.isArray(first.value.ALL)).toBe(true);
    expect(_.every(first.value.ALL, saga => _.startsWith(saga.FORK.fn.name, 'watch'))).toBe(true);
    expect(last.done).toBe(true);
  });
});
