import { delay } from 'redux-saga';
import { drain } from 'gnar-edge';

import { ConsoleMock, FetchMock, LocalStorageMock, requestFailure } from 'mocks';
import { accessToken, userId, email, token } from 'mocks/constants';
import activateActions from 'actions/activate';
import configureStore from 'js/redux/configureStore';

describe('redux activate', () => {
  it('properly updates the store and redirects to set-password on successful submitActivation', drain(function* () {
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const fetchMock = new FetchMock({ userId, accessToken });
    const localStorageMock = new LocalStorageMock();
    yield store.dispatch(activateActions.submitActivation(token));
    yield delay(2000);
    const body = JSON.stringify({ token });
    expect(fetchMock.fn).toHaveBeenCalledWith('/user/activate', { method: 'POST', body });
    expect(store.getState().activate.toJSON()).toEqual({ email, userId });
    expect(localStorage.setItem).toHaveBeenCalledWith('jwt', accessToken);
    expect(historyMock.replace).toHaveBeenCalledWith('/set-password');
    fetchMock.deactivate();
    localStorageMock.deactivate();
  }));

  it('propagates an error to the store when submitActivation fails', drain(function* () {
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const error = 'Invite has expired.';
    const consoleMock = new ConsoleMock();
    const fetchMock = new FetchMock({ error });
    const localStorageMock = new LocalStorageMock();
    yield store.dispatch(activateActions.submitActivation(token));
    yield delay(2000);
    const body = JSON.stringify({ token });
    expect(fetchMock.fn).toHaveBeenCalledWith('/user/activate', { method: 'POST', body });
    expect(consoleMock.error).toHaveBeenCalledWith(error);
    expect(store.getState().activate.toJSON()).toEqual({ error });
    expect(localStorage.setItem).not.toHaveBeenCalled();
    consoleMock.deactivate();
    fetchMock.deactivate();
    localStorageMock.deactivate();
  }));

  it('propagates an error to the store when the request fails', drain(function* () {
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const consoleMock = new ConsoleMock();
    const fetchMock = new FetchMock({}, { fail: true });
    const localStorageMock = new LocalStorageMock();
    yield store.dispatch(activateActions.submitActivation(token));
    yield delay(2000);
    const body = JSON.stringify({ token });
    expect(fetchMock.fn).toHaveBeenCalledWith('/user/activate', { method: 'POST', body });
    expect(consoleMock.error).toHaveBeenCalledWith(requestFailure);
    expect(store.getState().activate.toJSON()).toEqual({ error: requestFailure.message });
    expect(localStorage.setItem).not.toHaveBeenCalled();
    consoleMock.deactivate();
    fetchMock.deactivate();
    localStorageMock.deactivate();
  }));
});
