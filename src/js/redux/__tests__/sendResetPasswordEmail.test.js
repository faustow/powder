import { drain } from 'gnar-edge';

import { FetchMock } from 'mocks';
import { email, reCaptchaResponse } from 'mocks/constants';
import configureStore from 'js/redux/configureStore';
import sendResetPasswordEmailActions from 'actions/sendResetPasswordEmail';

describe('redux sendResetPasswordEmail', () => {
  it('submits the correct request on sendResetPasswordEmail', drain(function* () {
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const fetchMock = new FetchMock();
    yield store.dispatch(sendResetPasswordEmailActions.sendResetPasswordEmail(email, reCaptchaResponse));
    const body = JSON.stringify({ email, reCaptchaResponse });
    expect(fetchMock.fn)
      .toHaveBeenCalledWith('/user/send-reset-password-email', { method: 'POST', body });
    fetchMock.deactivate();
  }));
});
