import { delay } from 'redux-saga';
import { drain } from 'gnar-edge';

import { ConsoleMock, FetchMock, LocalStorageMock, requestFailure } from 'mocks';
import { accountDetails, email, jwt, password, reCaptchaResponse } from 'mocks/constants';
import configureStore from 'js/redux/configureStore';
import loginActions from 'actions/login';

describe('redux login', () => {
  it('properly updates the store and redirects to account on successful login', drain(function* () {
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const fetchMock = new FetchMock(accountDetails, { withAuthResponseHeader: true });
    const localStorageMock = new LocalStorageMock();
    yield store.dispatch(loginActions.submitLogin(email, password, reCaptchaResponse));
    const body = JSON.stringify({ email, password, reCaptchaResponse });
    expect(fetchMock.fn).toHaveBeenCalledWith('/user/login', { method: 'POST', body });
    expect(store.getState().accountDetails.toJSON()).toEqual(accountDetails);
    expect(localStorage.setItem).toHaveBeenCalledWith('jwt', jwt);
    expect(historyMock.replace).toHaveBeenCalledWith('/account');
    fetchMock.deactivate();
    localStorageMock.deactivate();
  }));

  it('propagates an error to the store when login fails', drain(function* () {
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const error = 'Invalid email or password.';
    const consoleMock = new ConsoleMock();
    const fetchMock = new FetchMock({ error });
    const localStorageMock = new LocalStorageMock();
    yield store.dispatch(loginActions.submitLogin(email, password, reCaptchaResponse));
    expect(consoleMock.error).toHaveBeenCalledWith(error);
    expect(store.getState().login.get('error')).toEqual(error);
    expect(localStorage.setItem).not.toHaveBeenCalled();
    yield delay(2000);
    expect(store.getState().login.get('error')).toEqual(null);
    consoleMock.deactivate();
    fetchMock.deactivate();
    localStorageMock.deactivate();
  }));

  it('propagates an error to the store when the request fails', drain(function* () {
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const consoleMock = new ConsoleMock();
    const fetchMock = new FetchMock({}, { fail: true });
    const localStorageMock = new LocalStorageMock();
    yield store.dispatch(loginActions.submitLogin(email, password, reCaptchaResponse));
    expect(consoleMock.error).toHaveBeenCalledWith(requestFailure);
    expect(store.getState().login.get('error')).toEqual(requestFailure.message);
    expect(localStorage.setItem).not.toHaveBeenCalled();
    yield delay(2000);
    expect(store.getState().login.get('error')).toEqual(null);
    consoleMock.deactivate();
    fetchMock.deactivate();
    localStorageMock.deactivate();
  }));
});
