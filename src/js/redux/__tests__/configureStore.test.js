describe('redux configureStore', () => {
  beforeEach(() => {
    delete process.env.NODE_ENV;
    jest.resetModules();
  });

  it('works in development mode', () => {
    process.env.NODE_ENV = 'development';
    const configureStore = require('js/redux/configureStore').default;  // eslint-disable-line global-require
    const store = configureStore();
    expect(new Set(Object.getOwnPropertyNames(store)))
      .toEqual(new Set(['dispatch', 'getState', 'replaceReducer', 'subscribe']));
  });

  it('works in production mode', () => {
    process.env.NODE_ENV = 'production';
    const configureStore = require('js/redux/configureStore').default;  // eslint-disable-line global-require
    const store = configureStore();
    expect(new Set(Object.getOwnPropertyNames(store)))
      .toEqual(new Set(['dispatch', 'getState', 'replaceReducer', 'subscribe']));
  });

  it('works in test mode', () => {
    process.env.NODE_ENV = 'test';
    const configureStore = require('js/redux/configureStore').default;  // eslint-disable-line global-require
    const store = configureStore();
    expect(new Set(Object.getOwnPropertyNames(store)))
      .toEqual(new Set(['dispatch', 'getState', 'replaceReducer', 'subscribe']));
  });
});
