import request from './request';

// [Project Gnar]: Only add new user requests to this file; start a new file under apis for new services.

const userRequest = _.partial(request, 'user');

export default {
  activate: token => userRequest('activate', 'POST', { token }, { toastErrors: false }),
  get: email => userRequest('get', 'POST', { email }, { auth: true }),
  login: (email, password, reCaptchaResponse) =>
    userRequest('login', 'POST', { email, password, reCaptchaResponse }, { auth: true, toastErrors: false }),
  resetPassword: (actionId, email, password) =>
    userRequest('reset-password', 'POST', { actionId, email, password }, { toastErrors: false }),
  scorePassword: password => userRequest('score-password', 'POST', { password }),
  sendResetPasswordEmail: (email, reCaptchaResponse) =>
    userRequest('send-reset-password-email', 'POST', { email, reCaptchaResponse }, { toastErrors: false }),
  signup: (email, reCaptchaResponse) => userRequest('signup', 'POST', { email, reCaptchaResponse }),
  update: (userId, data) => userRequest(userId, 'PUT', data, { auth: true })
};
