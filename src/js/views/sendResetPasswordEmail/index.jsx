import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReCaptcha from 'react-google-recaptcha';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import blue from '@material-ui/core/colors/blue';
import classnames from 'classnames';
import handleChange from 'gnar-edge/es/handleChange';
import withStyles from '@material-ui/core/styles/withStyles';

import { sendResetPasswordEmailActions } from 'actions';
import CardHeader from 'components/cardHeader';
import i18n from 'i18n';
import secureEmail from 'assets/images/secure-email.png';

import './index.scss';

const mapStateToProps = state => ({
  language: state.language.get('current')
});

const mapDispatchToProps = sendResetPasswordEmailActions;

const styles = theme => ({
  imgContainer: {
    backgroundColor: theme.palette.grey['400']
  },
  imgContainerSubmitted: {
    backgroundColor: blue['600']
  }
});

@withStyles(styles)
@connect(mapStateToProps, mapDispatchToProps)
export default class SendResetPasswordEmailView extends Component {
  static propTypes = {
    classes: PropTypes.shape({
      imgContainer: PropTypes.string.isRequired,
      imgContainerSubmitted: PropTypes.string.isRequired
    }).isRequired,
    language: PropTypes.string.isRequired,
    sendResetPasswordEmail: PropTypes.func.isRequired
  };

  state = {
    email: '',
    submitted: false
  };

  handleChange = this::handleChange;

  executeCaptcha = e => {
    this.recaptchaInstance.execute();
    e.preventDefault();
  }

  sendResetPasswordEmail = reCaptchaResponse => {
    this.setState({ submitted: true });
    this.props.sendResetPasswordEmail(this.state.email, reCaptchaResponse);
  };

  render() {
    const { classes, language } = this.props;
    const { email, submitted } = this.state;
    const {
      fieldLabels: { emailLabel },
      navButtons: { doneButtonText, submitButtonText },
      sendResetPasswordEmail: { cardTitle, content }
    } = i18n[language];
    return (
      <Grid container className='reset-password card-container' justify='center'>
        <Grid item lg={4} md={6} sm={8} xs={12}>
          <Card>
            <form onSubmit={this.executeCaptcha}>
              <CardHeader align='center' title={cardTitle} />
              <Divider />
              <CardContent>
                <Grid container>
                  <Grid item className='email' xs={12}>
                    <TextField
                      autoFocus
                      fullWidth
                      disabled={submitted}
                      id='email'
                      label={emailLabel}
                      margin='normal'
                      type='email'
                      onChange={this.handleChange('email')}
                    />
                  </Grid>
                  <Grid item sm={4} xs={12}>
                    <div
                      className={classnames('img-container', {
                        [classes.imgContainer]: !submitted,
                        [classes.imgContainerSubmitted]: submitted
                      })}
                    >
                      <img alt='Secure Email' src={secureEmail} />
                    </div>
                  </Grid>
                  <Grid item sm={8} xs={12}>
                    {content[submitted ? 'submitted' : 'initial'].map(s =>
                      <Typography key={s} paragraph>{s}</Typography>)
                    }
                  </Grid>
                </Grid>
              </CardContent>
              <CardActions>
                <Button
                  className='submit-button'
                  color='primary'
                  disabled={!email || submitted}
                  size='small'
                  type='submit'
                >{submitted ? doneButtonText : submitButtonText}
                </Button>
              </CardActions>
            </form>
            <ReCaptcha
              ref={el => { this.recaptchaInstance = el; }}
              sitekey={process.env.RECAPTCHA_SITE_KEY}
              size='invisible'
              onChange={this.sendResetPasswordEmail}
            />
          </Card>
        </Grid>
      </Grid>
    );
  }
}
