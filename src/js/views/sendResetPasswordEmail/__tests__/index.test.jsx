import { Map } from 'immutable';
import { StaticRouter } from 'react-router';
import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';

import { ENGLISH } from 'i18n';
import { SEND_RESET_PASSWORD_EMAIL } from 'actions/sendResetPasswordEmail';
import { email, reCaptchaResponse } from 'mocks/constants';
import SendResetPasswordEmailView from 'views/sendResetPasswordEmail';


const mockStore = configureStore();

const getRootComponent = () => {
  const store = mockStore({ language: Map({ current: ENGLISH }) });
  const root = mount((
    <StaticRouter context={{}}>
      <SendResetPasswordEmailView store={store} />
    </StaticRouter>
  ));
  return { root, store };
};

describe('<SendResetPasswordEmailView />', () => {
  it('renders properly', () => {
    const { root } = getRootComponent();
    const emailTextField = root.find('TextField#email');
    const emailLabel = emailTextField.find('label');
    const submitButton = root.find('Button');
    expect(emailTextField).toHaveLength(1);
    expect(emailTextField.find('input')).toHaveLength(1);
    expect(emailLabel).toHaveLength(1);
    expect(emailLabel.text()).toBe('Email');
    expect(submitButton).toHaveLength(1);
    expect(submitButton.text()).toBe('Submit');
    expect(submitButton.prop('disabled')).toBe(true);
  });

  it('has an enabled submit button when the email input has a value', () => {
    const { root } = getRootComponent();
    root.find('TextField#email').find('input').simulate('change', { target: { value: email } });
    expect(root.update().find('Button').prop('disabled')).toBe(false);
  });

  it('dispatches the correct action when an email has been entered and the Submit button is clicked', () => {
    const { root, store } = getRootComponent();
    root.find('#email').find('input').simulate('change', { target: { value: email } });
    root.update().find('Button').simulate('submit');
    expect(store.getActions()).toEqual([{ type: SEND_RESET_PASSWORD_EMAIL, payload: { email, reCaptchaResponse } }]);
  });

  it('dispatches the correct action when an email has been entered and the Submit button is clicked', () => {
    const { root, store } = getRootComponent();
    root.find('#email').find('input').simulate('change', { target: { value: email } });
    root.update().find('form').simulate('submit');
    expect(store.getActions()).toEqual([{ type: SEND_RESET_PASSWORD_EMAIL, payload: { email, reCaptchaResponse } }]);
  });

  it('disables the Submit button and sets its text to Done after the form has been submitted', () => {
    const { root } = getRootComponent();
    root.find('#email').find('input').simulate('change', { target: { value: email } });
    root.update().find('Button').simulate('submit');
    const updatedSubmitButton = root.update().find('Button');
    expect(updatedSubmitButton.prop('disabled')).toBe(true);
    expect(updatedSubmitButton.text()).toBe('Done');
  });
});
