import { Map } from 'immutable';
import { StaticRouter } from 'react-router';
import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';

import { ENGLISH, english } from 'i18n';
import { SUBMIT_ACTIVATION } from 'actions/activate';
import { matchMock } from 'mocks';
import { token } from 'mocks/constants';
import ActivateView from 'views/activate';

const { activate: { defaultContent, errorContent } } = english;

const mockStore = configureStore();

const getRootComponent = initialState => {
  const store = mockStore({ activate: Map(), language: Map({ current: ENGLISH }), ...initialState });
  const routeMatch = _.chain(matchMock).cloneDeep().set('params.token', token).value();
  const root = mount((
    <StaticRouter context={{}}>
      <ActivateView match={routeMatch} store={store} />
    </StaticRouter>
  ));
  return { root, store };
};

describe('<ActivateView />', () => {
  it('renders properly', () => {
    const { root } = getRootComponent();
    const contentDiv = root.find('Grid[item=true]');
    expect(contentDiv).toHaveLength(1);
    expect(contentDiv.text()).toBe(defaultContent.join(''));
  });

  it('dispatches the correct action when the component mounts', () => {
    const { store } = getRootComponent();
    expect(store.getActions()).toEqual([{ type: SUBMIT_ACTIVATION, payload: { token } }]);
  });

  it('displays an error message when passed in from the store', () => {
    const error = 'Activation rejected by server.';
    const { root } = getRootComponent({ activate: Map({ error }) });
    expect(root.find('Grid[item=true]').text()).toBe(errorContent(error).join(''));
  });
});
