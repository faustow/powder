import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

import { activateActions } from 'actions';
import i18n from 'i18n';

const mapStateToProps = state => ({
  error: state.activate.get('error'),
  language: state.language.get('current')
});

const mapDispatchToProps = activateActions;

const styles = theme => ({
  activate: {
    color: theme.palette.primary.contrastText
  }
});

@withStyles(styles)
@connect(mapStateToProps, mapDispatchToProps)
export default class ActivateView extends Component {
  static propTypes = {
    classes: PropTypes.shape({
      activate: PropTypes.string.isRequired
    }).isRequired,
    error: PropTypes.string,
    language: PropTypes.string.isRequired,
    match: ReactRouterPropTypes.match.isRequired,
    submitActivation: PropTypes.func.isRequired
  };

  static defaultProps = {
    error: ''
  }

  componentDidMount() {
    const { match: { params: { token } } } = this.props;
    this.props.submitActivation(token);
  }

  render() {
    const { classes, error, language } = this.props;
    const { activate: { defaultContent, errorContent } } = i18n[language];
    const content = error ? errorContent(error) : defaultContent;
    return (
      <Grid container className='card-container'>
        <Grid item>
          {content.map(s =>
            (
              <Typography
                key={s}
                paragraph
                align='center'
                className={classes.activate}
                variant='headline'
              >
                {s}
              </Typography>
            ))
          }
        </Grid>
      </Grid>
    );
  }
}
