import { connect } from 'react-redux';
import { notificationActions } from 'gnar-edge/es/notifications';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Link from 'react-router-dom/Link';
import PropTypes from 'prop-types';
import ReCaptcha from 'react-google-recaptcha';
import React, { Component } from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import handleChange from 'gnar-edge/es/handleChange';

import { loginActions } from 'actions';
import CardHeader from 'components/cardHeader';
import Overlay from 'components/overlay';
import i18n from 'i18n';

const mapStateToProps = state => ({
  error: state.login.get('error'),
  language: state.language.get('current')
});

const mapDispatchToProps = { ...loginActions, ...notificationActions };

@connect(mapStateToProps, mapDispatchToProps)
export default class LoginView extends Component {
  static propTypes = {
    error: PropTypes.string,
    history: ReactRouterPropTypes.history.isRequired,
    language: PropTypes.string.isRequired,
    submitLogin: PropTypes.func.isRequired
  };

  static defaultProps = {
    error: ''
  }

  state = {
    email: '',
    loading: false,
    password: ''
  };

  handleChange = this::handleChange;

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ loading: false });
      this.recaptchaInstance.reset();
    }
  }

  executeCaptcha = e => {
    this.recaptchaInstance.execute();
    e.preventDefault();
  }

  submitLogin = reCaptchaResponse => {
    this.setState({ loading: true });
    this.props.submitLogin(this.state.email, this.state.password, reCaptchaResponse);
  };

  render() {
    const { error, history, language } = this.props;
    const { loading } = this.state;
    const {
      fieldLabels: { emailLabel, passwordLabel },
      login: { cardTitle, forgotPassword },
      navButtons: { loginButtonText, signupButtonText }
    } = i18n[language];
    return (
      <Grid container className='login card-container' justify='center'>
        <Grid item lg={4} md={6} sm={8} xs={12}>
          <Card>
            <form onSubmit={this.executeCaptcha}>
              <CardHeader align='center' title={cardTitle} />
              <Divider />
              <CardContent>
                <TextField
                  autoFocus
                  fullWidth
                  disabled={loading}
                  id='email'
                  label={emailLabel}
                  margin='normal'
                  type='email'
                  onChange={this.handleChange('email')}
                />
                <TextField
                  fullWidth
                  disabled={loading}
                  id='password'
                  label={passwordLabel}
                  margin='normal'
                  type='password'
                  onChange={this.handleChange('password')}
                />
                {error
                  ? (
                    <Typography paragraph color='error'>
                      {error}
                    </Typography>
                  )
                  : null}
                <Typography>
                  <Link to='send-reset-password-email'>{forgotPassword}</Link>
                </Typography>
              </CardContent>
              <CardActions>
                <Grid container>
                  <Grid item align='left' xs={6}>
                    <Button
                      className='submit-button'
                      color='primary'
                      disabled={!this.state.email || !this.state.password}
                      size='small'
                      type='submit'
                    >{loginButtonText}
                    </Button>
                  </Grid>
                  <Grid item align='right' xs={6}>
                    <Button
                      className='submit-button'
                      color='primary'
                      disabled={!!this.state.email || !!this.state.password}
                      size='small'
                      onClick={() => { history.push('signup'); }}
                    >{signupButtonText}
                    </Button>
                  </Grid>
                </Grid>
              </CardActions>
            </form>
            <ReCaptcha
              ref={el => { this.recaptchaInstance = el; }}
              sitekey={process.env.RECAPTCHA_SITE_KEY}
              size='invisible'
              onChange={this.submitLogin}
            />
          </Card>
          {loading ? <Overlay /> : null}
        </Grid>
      </Grid>
    );
  }
}
