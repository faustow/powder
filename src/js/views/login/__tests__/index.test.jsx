import { Map } from 'immutable';
import { StaticRouter } from 'react-router';
import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';

import { ENGLISH } from 'i18n';
import { SUBMIT_LOGIN } from 'actions/login';
import { LOGIN_ERROR } from 'reducers/login';
import { email, password, reCaptchaResponse } from 'mocks/constants';
import { historyMock } from 'mocks';
import LoginView from 'views/login';

const mockStore = configureStore();

const baseState = { language: Map({ current: ENGLISH }) };

const getRootComponent = initialState => {
  const effectiveState = _.isFunction(initialState) ? initialState : { ...baseState, login: Map(), ...initialState };
  const store = mockStore(effectiveState);
  const root = mount((
    <StaticRouter context={{}}>
      <LoginView history={historyMock} store={store} />
    </StaticRouter>
  ));
  return { root, store };
};

describe('<LoginView />', () => {
  it('renders properly', () => {
    const { root } = getRootComponent();
    const emailTextField = root.find('TextField#email');
    const emailLabel = emailTextField.find('label');
    const passwordTextField = root.find('TextField#password');
    const passwordLabel = passwordTextField.find('label');
    const resetLink = root.find('Link');
    const submitButton = root.find('Button[type="submit"]');
    const signupButton = root.find('Button[type="button"]');
    const reCAPTCHA = root.find('ReCAPTCHA');
    expect(emailTextField).toHaveLength(1);
    expect(emailTextField.prop('disabled')).toBe(false);
    expect(emailTextField.find('input')).toHaveLength(1);
    expect(emailLabel).toHaveLength(1);
    expect(emailLabel.text()).toBe('Email');
    expect(passwordTextField).toHaveLength(1);
    expect(passwordTextField.prop('disabled')).toBe(false);
    expect(passwordTextField.find('input')).toHaveLength(1);
    expect(passwordLabel).toHaveLength(1);
    expect(passwordLabel.text()).toBe('Password');
    expect(resetLink).toHaveLength(1);
    expect(resetLink.text()).toBe('Forgot your password?');
    expect(resetLink.prop('to')).toBe('send-reset-password-email');
    expect(submitButton).toHaveLength(1);
    expect(submitButton.text()).toBe('Login');
    expect(submitButton.prop('disabled')).toBe(true);
    expect(signupButton.text()).toBe('Signup');
    expect(signupButton.prop('disabled')).toBe(false);
    expect(reCAPTCHA).toHaveLength(1);
    expect(reCAPTCHA.prop('onChange')).toBe(root.find('LoginView').instance().submitLogin);
    expect(root.find('.overlay')).toHaveLength(0);
  });

  it('has a disabled submit button on render', () => {
    const { root } = getRootComponent();
    expect(root.find('Button[type="submit"]').prop('disabled')).toBe(true);
  });

  it('has a disabled submit button when only the email input has a value', () => {
    const { root } = getRootComponent();
    root.find('#email').find('input').simulate('change', { target: { value: email } });
    expect(root.update().find('Button[type="submit"]').prop('disabled')).toBe(true);
  });

  it('has a disabled submit button when only the password input has a value', () => {
    const { root } = getRootComponent();
    root.find('#password').find('input').simulate('change', { target: { value: password } });
    expect(root.update().find('Button[type="submit"]').prop('disabled')).toBe(true);
  });

  it('has an enabled submit button when the input and password inputs have values', () => {
    const { root } = getRootComponent();
    root.find('#email').find('input').simulate('change', { target: { value: email } });
    root.find('#password').find('input').simulate('change', { target: { value: password } });
    expect(root.find('Button[type="submit"]').prop('disabled')).toBe(false);
  });

  it(
    'dispatches the correct action when an email and password have been entered and the Login button is clicked',
    () => {
      const { root, store } = getRootComponent();
      root.find('#email').find('input').simulate('change', { target: { value: email } });
      root.find('#password').find('input').simulate('change', { target: { value: password } });
      root.update().find('Button[type="submit"]').simulate('submit');
      expect(store.getActions()).toEqual([{ type: SUBMIT_LOGIN, payload: { email, password, reCaptchaResponse } }]);
    }
  );

  it('dispatches the correct action when an email and password have been entered and the form is submitted', () => {
    const { root, store } = getRootComponent();
    root.find('#email').find('input').simulate('change', { target: { value: email } });
    root.find('#password').find('input').simulate('change', { target: { value: password } });
    root.update().find('form').simulate('submit');
    expect(store.getActions()).toEqual([{ type: SUBMIT_LOGIN, payload: { email, password, reCaptchaResponse } }]);
  });

  it('disables the input fields and displays an overlay when the login action has been dispatched', () => {
    const { root } = getRootComponent();
    root.find('#email').find('input').simulate('change', { target: { value: email } });
    root.find('#password').find('input').simulate('change', { target: { value: password } });
    root.update().find('Button[type="submit"]').simulate('submit');
    expect(root.find('TextField#email').prop('disabled')).toBe(true);
    expect(root.find('TextField#password').prop('disabled')).toBe(true);
    expect(root.find('.overlay')).toHaveLength(1);
  });

  it('displays an error message when passed in from the store', () => {
    const error = 'Invalid email or password.';
    const { root } = getRootComponent({ login: Map({ error }) });
    expect(root.update().find('Typography').findWhere(n => n.prop('color') === 'error').text()).toBe(error);
  });

  it('enables the input fields and hides the overlay when an error message is passed in from the store', () => {
    const error = 'Invalid email or password.';
    let initialState = true;
    const storeState = () => ({ ...baseState, login: Map({ error: initialState ? '' : error }) });
    const { root, store } = getRootComponent(storeState);
    expect(root.find('TextField#email').prop('disabled')).toBe(false);
    expect(root.find('TextField#password').prop('disabled')).toBe(false);
    expect(root.find('.overlay')).toHaveLength(0);
    root.find('#email').find('input').simulate('change', { target: { value: email } });
    root.find('#password').find('input').simulate('change', { target: { value: password } });
    root.update().find('form').simulate('submit');
    expect(root.find('TextField#email').prop('disabled')).toBe(true);
    expect(root.find('TextField#password').prop('disabled')).toBe(true);
    expect(root.find('.overlay')).toHaveLength(1);
    initialState = false;
    store.dispatch({ type: LOGIN_ERROR, payload: { error } });
    const updatedRoot = root.update();
    expect(updatedRoot.find('TextField#email').prop('disabled')).toBe(false);
    expect(updatedRoot.find('TextField#password').prop('disabled')).toBe(false);
    expect(updatedRoot.find('.overlay')).toHaveLength(0);
  });

  it('does not update loading state when new props are received and error is empty', () => {
    const error = 'Invalid email or password.';
    let state = 0;
    const storeState = () => ({ ...baseState, login: Map({ error: state === 1 ? error : '' }) });
    const { root, store } = getRootComponent(storeState);
    expect(root.find('.overlay')).toHaveLength(0);
    root.find('#email').find('input').simulate('change', { target: { value: email } });
    root.find('#password').find('input').simulate('change', { target: { value: password } });
    root.update().find('form').simulate('submit');
    expect(root.find('.overlay')).toHaveLength(1);
    state = 1;
    store.dispatch({ type: LOGIN_ERROR, payload: { error } });
    expect(root.update().find('.overlay')).toHaveLength(0);
    state = 2;
    store.dispatch({ type: LOGIN_ERROR, payload: { error: '' } });
    expect(root.update().find('.overlay')).toHaveLength(0);
  });

  it('navigates to signup when the Signup button is clicked', () => {
    const { root } = getRootComponent();
    root.find('Button[type="button"]').simulate('click');
    expect(historyMock.push).toHaveBeenCalledWith('signup');
  });
});
