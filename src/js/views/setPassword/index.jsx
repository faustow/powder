import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/InfoOutlined';
import Popover from '@material-ui/core/Popover';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import base64 from 'gnar-edge/es/base64';
import classnames from 'classnames';
import green from '@material-ui/core/colors/green';
import handleChange from 'gnar-edge/es/handleChange';
import withStyles from '@material-ui/core/styles/withStyles';

import { passwordScoreActions, resetPasswordActions, setPasswordActions } from 'actions';
import CardHeader from 'components/cardHeader';
import i18n from 'i18n';

import './index.scss';

const mapStateToProps = state => ({
  apiError: state.setPassword.get('error'),
  email: state.activate.get('email'),
  language: state.language.get('current'),
  passwordScore: state.passwordScore.toJSON(),
  pwnedPasswordCount: state.pwnedPasswordCount.toJSON(),
  userId: state.activate.get('userId')
});

const mapDispatchToProps = { ...passwordScoreActions, ...resetPasswordActions, ...setPasswordActions };

const styles = theme => ({
  error: {
    color: theme.palette.error.main
  },
  popoverTypography: {
    margin: theme.spacing.unit * 2
  },
  success: {
    color: green['600']
  },
  working: {
    background: theme.palette.grey['400']
  }
});

@withStyles(styles)
@connect(mapStateToProps, mapDispatchToProps)
export default class SetPasswordView extends Component {
  static propTypes = {
    apiError: PropTypes.string,
    classes: PropTypes.shape({
      error: PropTypes.string.isRequired,
      popoverTypography: PropTypes.string.isRequired,
      success: PropTypes.string.isRequired
    }).isRequired,
    email: PropTypes.string,
    getPasswordScore: PropTypes.func.isRequired,
    history: ReactRouterPropTypes.history.isRequired,
    language: PropTypes.string.isRequired,
    match: ReactRouterPropTypes.match,
    passwordScore: PropTypes.shape({
      feedback: PropTypes.shape({
        warning: PropTypes.string
      }),
      passwordScoreDispatchedAt: PropTypes.number,
      score: PropTypes.number
    }).isRequired,
    pwnedPasswordCount: PropTypes.shape({
      count: PropTypes.number,
      passwordScoreDispatchedAt: PropTypes.number
    }).isRequired,
    resetPassword: PropTypes.func.isRequired,
    setPassword: PropTypes.func.isRequired,
    userId: PropTypes.string
  };

  static defaultProps = {
    apiError: '',
    email: '',
    match: { params: {} },
    userId: ''
  }

  state = {
    confirmPassword: '',
    confirmPasswordError: '',
    email: '',
    password: '',
    popoverOpen: false
  };

  handleChange = this::handleChange;

  componentWillMount() {
    const { history, match: { params: { token } } } = this.props;
    if (token) {
      const [email, actionId] = base64.decode(token).split('/');
      this.setState({ actionId, email });
    } else {
      const { userId, email } = this.props;
      if (!userId || !email) {
        history.replace('/login');
      }
      this.setState({ email });
    }
  }

  componentDidUpdate = () => {
    const { passwordScore: { passwordScoreDispatchedAt, score }, pwnedPasswordCount } = this.props;
    if (passwordScoreDispatchedAt === this.lastPasswordScoreDispatchedAt &&
        (score < 4 || pwnedPasswordCount.passwordScoreDispatchedAt === this.lastPasswordScoreDispatchedAt)) {
      clearTimeout(this.timeout);
    }
  }

  shouldComponentUpdate = (nextProps, nextState) =>
    (!this.isInWaitState(nextProps, nextState) || new Date().getTime() - this.lastPasswordScoreDispatchedAt > 250) &&
    (!_.isEqual(this.props, nextProps) || !_.isEqual(this.state, nextState));

  getPasswordHelper = ({ error, info, success, warning }) => {
    const { classes, language } = this.props;
    const { popoverOpen } = this.state;
    const { setPassword: { workingMessage } } = i18n[language];
    return [
      {
        test: this.isInWaitState,
        result: () => {
          const Bar = () => <div className={classes.working} />;
          return (
            <div className='working-container'>
              <div className='working'><Bar /><Bar /><Bar /></div>
              <Typography variant='caption'>{workingMessage}</Typography>
            </div>
          );
        }
      },
      {
        test: () => warning,
        result: () => (
          <div className={classnames(classes.error, 'icon-button-container')}>
            <IconButton
              buttonRef={node => { this.anchorEl = node; }}
              className='info-icon'
              onClick={() => { this.handleChange('popoverOpen')(true); }}
            >
              <InfoIcon />
            </IconButton>
            <Typography className='pwned-info' color='error' variant='caption'>{info}</Typography>
            <Popover
              anchorEl={this.anchorEl}
              anchorOrigin={{
                vertical: 'center',
                horizontal: 'center'
              }}
              open={popoverOpen}
              transformOrigin={{
                vertical: 'bottom',
                horizontal: 'left'
              }}
              onClose={() => { this.handleChange('popoverOpen')(false); }}
            >
              <Typography className={classes.popoverTypography}>{warning}</Typography>
            </Popover>
          </div>
        )
      },
      {
        test: () => true,
        result: () => (
          <Typography
            className={classnames({
              [classes.error]: error,
              [classes.success]: success
            })}
            variant='caption'
          >
            {info}
          </Typography>
        )
      }
    ]
      .find(item => item.test()).result();
  }

  getPasswordState() {
    // Note: To test a password that has a score of 4 but has been pwned, try 'gimnasto4ka2012'.
    const { apiError, language, passwordScore, pwnedPasswordCount } = this.props;
    const { password } = this.state;
    const { count } = pwnedPasswordCount;
    const { score } = passwordScore;
    const { setPassword: { errors, passwordHelperText, successMessage } } = i18n[language];
    return {
      error: false,
      success: false,
      ...[
        { test: () => apiError, result: () => ({ error: true, info: apiError }) },
        { test: () => / /.test(password), result: () => ({ error: true, info: errors.REQUIRE_NO_SPACES }) },
        { test: () => password.length < 8, result: () => ({ info: passwordHelperText }) },
        { test: this.isInWaitState, result: () => ({}) },
        {
          test: () => score < 4 && passwordScore.feedback.warning,
          result: () => ({
            error: true,
            info: `Strength: ${score} / 4`,
            warning: passwordScore.feedback.warning
          })
        },
        {
          test: () => score < 4,
          result: () => ({ error: true, info: errors.SCORE_TOO_LOW(score) })
        },
        {
          test: () => pwnedPasswordCount.count > 0,
          result: () => ({
            error: true,
            info: errors.PASSWORD_MATCHES_HACKED_ACCOUNTS(count),
            warning: (
              <span>
                Stats courtesy&nbsp;
                <a
                  href='https://haveibeenpwned.com/passwords'
                  rel='noopener noreferrer'
                  target='_blank'
                >
                  haveibeenpwned.com
                </a>
              </span>)
          })
        },
        { test: () => true, result: () => ({ info: successMessage, success: true }) }
      ]
        .find(item => item.test()).result()
    };
  }

  handlePasswordChange = () => {
    const { getPasswordScore } = this.props;
    const { password } = this.state;
    if (password.length > 7) {
      this.lastPasswordScoreDispatchedAt = new Date().getTime();
      getPasswordScore(password, this.lastPasswordScoreDispatchedAt);
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => { this.forceUpdate(); }, 250);
    }
  }

  handleFocus = name => () => {
    this.setState({ [name]: '' });
  }

  isInWaitState = (props, state) => {
    const { passwordScore, pwnedPasswordCount } = props || this.props;
    const { password } = state || this.state;
    return (
      !/ /.test(password) && password.length > 7 && (
        passwordScore.passwordScoreDispatchedAt !== this.lastPasswordScoreDispatchedAt || (
          passwordScore.score > 3 &&
          pwnedPasswordCount.passwordScoreDispatchedAt !== this.lastPasswordScoreDispatchedAt
        )
      )
    );
  }

  updatePassword = e => {
    const { actionId, email, password } = this.state;
    if (actionId) {
      this.props.resetPassword(actionId, email, password);
    } else {
      this.props.setPassword(this.props.userId, password);
    }
    e.preventDefault();
  }

  validateConfirmPassword = () => {
    const { language } = this.props;
    const { confirmPassword, password } = this.state;
    const { setPassword: { errors } } = i18n[language];
    const confirmPasswordError = password && confirmPassword && password !== confirmPassword
      ? errors.PASSWORDS_MUST_MATCH
      : '';
    this.setState({ confirmPasswordError });
  }

  render() {
    const { apiError, language } = this.props;
    const {
      confirmPassword,
      confirmPasswordError,
      email,
      password
    } = this.state;
    const passwordState = this.getPasswordState();
    const { error, success, warning } = passwordState;
    const confirmPasswordHasError = success && (!!apiError || !!confirmPasswordError);
    const {
      fieldLabels: { confirmPasswordLabel, emailLabel, passwordLabel },
      navButtons: { submitButtonText },
      setPassword: { cardTitle, pleaseSetPassword }
    } = i18n[language];
    return (
      <Grid container className='set-password card-container' justify='center'>
        <Grid item lg={4} md={6} sm={8} xs={12}>
          <Card>
            <form onSubmit={this.updatePassword}>
              <CardHeader align='center' title={cardTitle} />
              <Divider />
              <CardContent>
                <Typography>
                  {pleaseSetPassword}
                </Typography>
                <TextField
                  disabled
                  fullWidth
                  id='email'
                  label={emailLabel}
                  margin='normal'
                  value={email}
                />
                <TextField
                  autoFocus
                  fullWidth
                  error={error}
                  id='password'
                  label={passwordLabel}
                  margin='normal'
                  type='password'
                  onChange={this.handleChange('password', this.handlePasswordChange)}
                />
                {this.getPasswordHelper(passwordState)}
                <TextField
                  fullWidth
                  className={classnames('confirm-password', { 'after-warning': warning })}
                  error={confirmPasswordHasError}
                  helperText={success ? apiError || confirmPasswordError : ''}
                  id='confirm-password'
                  label={confirmPasswordLabel}
                  margin='normal'
                  type='password'
                  onBlur={this.validateConfirmPassword}
                  onChange={this.handleChange('confirmPassword')}
                  onFocus={this.handleFocus('confirmPasswordError')}
                />
              </CardContent>
              <CardActions>
                <Button
                  className={classnames('submit', { 'after-error': confirmPasswordHasError })}
                  color='primary'
                  disabled={
                    !password ||
                    !success ||
                    !!apiError ||
                    !!confirmPasswordError ||
                    password !== confirmPassword
                  }
                  size='small'
                  type='submit'
                >
                  {submitButtonText}
                </Button>
              </CardActions>
            </form>
          </Card>
        </Grid>
      </Grid>
    );
  }
}
