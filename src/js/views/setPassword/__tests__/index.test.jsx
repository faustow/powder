import { Map } from 'immutable';
import { StaticRouter } from 'react-router';
import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';

import { ENGLISH, english } from 'i18n';
import { GET_PASSWORD_SCORE } from 'actions/password/score';
import { RESET_PASSWORD } from 'actions/password/reset';
import { SET_PASSWORD } from 'actions/password/set';
import { actionId, userId, email, password, token } from 'mocks/constants';
import { historyMock, matchMock } from 'mocks';
import SetPasswordView from 'views/setPassword';

const { setPassword: { errors, passwordHelperText, successMessage, workingMessage } } = english;

const mockStore = configureStore();

const getRootComponent = (initialState, resetPasswordToken, historyReplaceMock) => {
  const store = mockStore({
    activate: Map({ userId, email }),
    language: Map({ current: ENGLISH }),
    passwordScore: Map(),
    pwnedPasswordCount: Map(),
    setPassword: Map(),
    ...initialState
  });
  const routeMatch = resetPasswordToken ? _.chain(matchMock).cloneDeep().set('params.token', token).value() : matchMock;
  historyReplaceMock && (historyMock.replace = historyReplaceMock);
  const root = mount((
    <StaticRouter context={{}} initialEntries={['/']}>
      <SetPasswordView history={historyMock} match={routeMatch} store={store} />
    </StaticRouter>
  ));
  return { root, store };
};

describe('<SetPasswordView />', () => {
  it("redirects to /login if there's no token and no userId / email", () => {
    const historyReplaceMock = jest.fn();
    getRootComponent({ activate: Map({ userId }) }, null, historyReplaceMock);
    expect(historyReplaceMock).toHaveBeenCalledWith('/login');
    historyReplaceMock.mockClear();
    expect(historyReplaceMock).not.toHaveBeenCalled();
    getRootComponent({ activate: Map({ email }) }, null, historyReplaceMock);
    expect(historyReplaceMock).toHaveBeenCalledWith('/login');
  });

  it('renders properly', () => {
    const { root } = getRootComponent();
    const emailTextField = root.find('TextField#email');
    const emailTextFieldInput = emailTextField.find('input');
    const emailLabel = emailTextField.find('label');
    const passwordTextField = root.find('TextField#password');
    const passwordHelperTextEl = root.find('CardContent').find('Typography').at(1);
    const passwordLabel = passwordTextField.find('label');
    const confirmPasswordTextField = root.find('TextField#confirm-password');
    const confirmPasswordLabel = confirmPasswordTextField.find('label');
    const submitButton = root.find('Button');
    expect(emailTextField).toHaveLength(1);
    expect(emailTextFieldInput).toHaveLength(1);
    expect(emailTextFieldInput.prop('value')).toBe(email);
    expect(emailLabel).toHaveLength(1);
    expect(emailLabel.text()).toBe('Email');
    expect(passwordTextField).toHaveLength(1);
    expect(passwordTextField.prop('error')).toBe(false);
    expect(passwordHelperTextEl.text()).toBe(passwordHelperText);
    expect(passwordTextField.find('input')).toHaveLength(1);
    expect(passwordLabel).toHaveLength(1);
    expect(passwordLabel.text()).toBe('Password');
    expect(confirmPasswordTextField).toHaveLength(1);
    expect(confirmPasswordTextField.prop('error')).toBe(false);
    expect(confirmPasswordTextField.prop('helperText')).toBe('');
    expect(confirmPasswordTextField.find('input')).toHaveLength(1);
    expect(confirmPasswordLabel).toHaveLength(1);
    expect(confirmPasswordLabel.text()).toBe('Confirm Password');
    expect(submitButton).toHaveLength(1);
    expect(submitButton.text()).toBe('Submit');
    expect(submitButton.prop('disabled')).toBe(true);
  });

  it('renders properly when a reset password token is passed in', () => {
    const { root } = getRootComponent({ activate: Map() }, token);
    expect(root.find('TextField#email').find('input').prop('value')).toBe(email);
  });

  it('has a disabled submit button on render', () => {
    const { root } = getRootComponent();
    expect(root.find('Button').prop('disabled')).toBe(true);
  });

  it('has a disabled submit button when only the password input has a value', () => {
    const { root } = getRootComponent();
    root.find('TextField#password').find('input').simulate('change', { target: { value: password } });
    expect(root.update().find('Button').prop('disabled')).toBe(true);
  });

  it('has a disabled submit button when only the confirm password input has a value', () => {
    const { root } = getRootComponent();
    root.find('TextField#confirm-password').find('input').simulate('change', { target: { value: password } });
    expect(root.update().find('Button').prop('disabled')).toBe(true);
  });

  it('does not validate an empty password', () => {
    const { root } = getRootComponent();
    const testPassword = '';
    root.find('TextField#password').find('input').simulate('change', { target: { value: testPassword } });
    const updatedRoot = root.update();
    const updatedPasswordTextField = updatedRoot.find('TextField#password');
    const updatedPasswordHelperTextEl = updatedRoot.find('CardContent').find('Typography').at(1);
    expect(updatedPasswordTextField.prop('error')).toBe(false);
    expect(updatedPasswordHelperTextEl.text()).toBe(passwordHelperText);
    expect(updatedRoot.find('Button').prop('disabled')).toBe(true);
  });

  it('validates the password length', () => {
    const { root } = getRootComponent();
    const testPassword = 'short';
    root.find('TextField#password').find('input').simulate('change', { target: { value: testPassword } });
    const updatedRoot = root.update();
    const updatedPasswordTextField = updatedRoot.find('TextField#password');
    const updatedPasswordHelperTextEl = updatedRoot.find('CardContent').find('Typography').at(1);
    expect(updatedPasswordTextField.prop('error')).toBe(false);
    expect(updatedPasswordHelperTextEl.text()).toBe(passwordHelperText);
    expect(updatedRoot.find('Button').prop('disabled')).toBe(true);
  });

  it('validates the password does not contain spaces', () => {
    const { root } = getRootComponent();
    const testPassword = 'with space';
    root.find('TextField#password').find('input').simulate('change', { target: { value: testPassword } });
    const updatedRoot = root.update();
    const updatedPasswordTextField = updatedRoot.find('TextField#password');
    const updatedPasswordHelperTextEl = updatedRoot.find('CardContent').find('Typography').at(1);
    expect(updatedPasswordTextField.prop('error')).toBe(true);
    expect(updatedPasswordHelperTextEl.text()).toBe(errors.REQUIRE_NO_SPACES);
    expect(updatedRoot.find('Button').prop('disabled')).toBe(true);
  });

  it('dispatches a score password action once the password is at least 8 characters', () => {
    const { root, store } = getRootComponent();
    root.find('TextField#password').find('input').simulate('change', { target: { value: password } });
    const actions = store.getActions();
    const { passwordScoreDispatchedAt } = actions[0].payload;
    expect(actions).toEqual([{ type: GET_PASSWORD_SCORE, payload: { password, passwordScoreDispatchedAt } }]);
    expect(new Date().getTime() - passwordScoreDispatchedAt).toBeLessThan(10);
  });

  it("displays a 'working' message 250ms after the score password action has been dispatched", () => {
    const { root } = getRootComponent();
    root.find('TextField#password').find('input').simulate('change', { target: { value: password } });
    return new Promise(resolve => {
      setTimeout(() => {
        const updatedRoot = root.update();
        expect(updatedRoot.find('.working')).toHaveLength(1);
        expect(updatedRoot.find('.working-container').find('Typography').text()).toBe(workingMessage);
        resolve();
      }, 250);
    });
  });

  it('displays the correct validation error when the password score is less than 4', () => {
    _.range(1).forEach(score => {
      const passwordScoreDispatchedAt = new Date().getTime();
      const { root } = getRootComponent({ passwordScore: Map({ passwordScoreDispatchedAt, score, feedback: {} }) });
      root.find('TextField#password').find('input').simulate('change', { target: { value: password } });
      root.find('TextField#confirm-password').find('input').simulate('change', { target: { value: password } });
      root.find('SetPasswordView').instance().lastPasswordScoreDispatchedAt = passwordScoreDispatchedAt;
      root.find('SetPasswordView').instance().forceUpdate();
      const updatedRoot = root.update();
      const passwordHelperTextEl = updatedRoot.find('CardContent').find('Typography').at(1);
      expect(passwordHelperTextEl.text()).toBe(errors.SCORE_TOO_LOW(score));
      expect(updatedRoot.find('Button').prop('disabled')).toBe(true);
      expect(updatedRoot.find('IconButton')).toHaveLength(0);
    });
  });

  it(
    'displays the correct validation error and an IconButton when the password score is less than 4 and has feedback',
    () => {
      _.range(4).forEach(score => {
        const passwordScoreDispatchedAt = new Date().getTime();
        const passwordScore = Map({ passwordScoreDispatchedAt, score, feedback: { warning: 'Danger!' } });
        const { root } = getRootComponent({ passwordScore });
        root.find('TextField#password').find('input').simulate('change', { target: { value: password } });
        root.find('SetPasswordView').instance().lastPasswordScoreDispatchedAt = passwordScoreDispatchedAt;
        root.find('SetPasswordView').instance().forceUpdate();
        const updatedRoot = root.update();
        const passwordHelperTextEl = updatedRoot.find('CardContent').find('Typography').at(1);
        expect(passwordHelperTextEl.text()).toBe(errors.SCORE_TOO_LOW(score));
        expect(updatedRoot.find('Button').prop('disabled')).toBe(true);
        expect(updatedRoot.find('IconButton')).toHaveLength(1);
      });
    }
  );

  it('displays the password score feedback warning in a Popover when the info IconButton is clicked', () => {
    const score = 3;
    const warning = "Don't dead open inside";
    const event = new KeyboardEvent('keydown', { keyCode: 27 /* Esc */ });
    const passwordScoreDispatchedAt = new Date().getTime();
    const passwordScore = Map({ passwordScoreDispatchedAt, score, feedback: { warning } });
    const { root } = getRootComponent({ passwordScore });
    root.find('TextField#password').find('input').simulate('change', { target: { value: password } });
    root.find('SetPasswordView').instance().lastPasswordScoreDispatchedAt = passwordScoreDispatchedAt;
    root.find('SetPasswordView').instance().forceUpdate();
    const updatedRoot = root.update();
    updatedRoot.find('IconButton').simulate('click');
    expect(updatedRoot.find('Popover')).toHaveLength(1);
    expect(updatedRoot.find('Popover').prop('open')).toBe(true);
    expect(updatedRoot.find('Popover').find('Typography').text()).toBe(warning);
    document.dispatchEvent(event);
    expect(updatedRoot.update().find('Popover').prop('open')).toBe(false);
  });

  it('displays the correct validation error when the password score is 4 and the pwnedPasswordCount > 0', () => {
    const count = 1;
    const passwordScoreDispatchedAt = new Date().getTime();
    const passwordScore = Map({ passwordScoreDispatchedAt, score: 4, feedback: {} });
    const pwnedPasswordCount = Map({ passwordScoreDispatchedAt, count });
    const { root } = getRootComponent({ passwordScore, pwnedPasswordCount });
    root.find('TextField#password').find('input').simulate('change', { target: { value: password } });
    root.find('TextField#confirm-password').find('input').simulate('change', { target: { value: password } });
    root.find('SetPasswordView').instance().lastPasswordScoreDispatchedAt = passwordScoreDispatchedAt;
    root.find('SetPasswordView').instance().forceUpdate();
    const updatedRoot = root.update();
    const passwordHelperTextEl = updatedRoot.find('CardContent').find('Typography').at(1);
    expect(passwordHelperTextEl.text()).toBe(errors.PASSWORD_MATCHES_HACKED_ACCOUNTS(count));
    expect(updatedRoot.find('Button').prop('disabled')).toBe(true);
  });

  it('displays a link to haveibeenpwned.com in a Popover when the info IconButton is clicked', () => {
    const event = new KeyboardEvent('keydown', { keyCode: 27 /* Esc */ });
    const passwordScoreDispatchedAt = new Date().getTime();
    const passwordScore = Map({ passwordScoreDispatchedAt, score: 4, feedback: {} });
    const pwnedPasswordCount = Map({ passwordScoreDispatchedAt, count: 9 });
    const { root } = getRootComponent({ passwordScore, pwnedPasswordCount });
    root.find('TextField#password').find('input').simulate('change', { target: { value: password } });
    root.find('SetPasswordView').instance().lastPasswordScoreDispatchedAt = passwordScoreDispatchedAt;
    root.find('SetPasswordView').instance().forceUpdate();
    const updatedRoot = root.update();
    updatedRoot.find('IconButton').simulate('click');
    expect(updatedRoot.find('Popover')).toHaveLength(1);
    expect(updatedRoot.find('Popover').prop('open')).toBe(true);
    expect(updatedRoot.find('Popover').find('a').text()).toBe('haveibeenpwned.com');
    document.dispatchEvent(event);
    expect(updatedRoot.update().find('Popover').prop('open')).toBe(false);
  });

  it('displays the success message when the password score is 4 and the pwnedPasswordCount is 0', () => {
    const passwordScoreDispatchedAt = new Date().getTime();
    const passwordScore = Map({ passwordScoreDispatchedAt, score: 4, feedback: {} });
    const pwnedPasswordCount = Map({ passwordScoreDispatchedAt, count: 0 });
    const { root } = getRootComponent({ passwordScore, pwnedPasswordCount });
    root.find('TextField#password').find('input').simulate('change', { target: { value: password } });
    root.find('SetPasswordView').instance().lastPasswordScoreDispatchedAt = passwordScoreDispatchedAt;
    root.find('SetPasswordView').instance().forceUpdate();
    const updatedRoot = root.update();
    const passwordHelperTextEl = updatedRoot.find('CardContent').find('Typography').at(1);
    expect(passwordHelperTextEl.text()).toBe(successMessage);
    expect(updatedRoot.find('Button').prop('disabled')).toBe(true);
  });

  it('validates that the password matches the confirm password', () => {
    const passwordScoreDispatchedAt = new Date().getTime();
    const passwordScore = Map({ passwordScoreDispatchedAt, score: 4, feedback: {} });
    const pwnedPasswordCount = Map({ passwordScoreDispatchedAt, count: 0 });
    const { root } = getRootComponent({ passwordScore, pwnedPasswordCount });
    const testConfirmPassword = 'Password1!x';
    root.find('TextField#password').find('input').simulate('change', { target: { value: password } });
    root.find('TextField#confirm-password').find('input').simulate('change', { target: { value: testConfirmPassword } })
      .simulate('blur');
    root.find('SetPasswordView').instance().lastPasswordScoreDispatchedAt = passwordScoreDispatchedAt;
    root.find('SetPasswordView').instance().forceUpdate();
    const updatedRoot = root.update();
    const updatedConfirmPasswordTextField = updatedRoot.find('TextField#confirm-password');
    expect(updatedConfirmPasswordTextField.prop('error')).toBe(true);
    expect(updatedConfirmPasswordTextField.prop('helperText')).toBe(errors.PASSWORDS_MUST_MATCH);
    expect(updatedRoot.find('Button').prop('disabled')).toBe(true);
  });

  it('does not display the confirm password validation message when the password confirmation input has focus', () => {
    const passwordScoreDispatchedAt = new Date().getTime();
    const passwordScore = Map({ passwordScoreDispatchedAt, score: 4, feedback: {} });
    const pwnedPasswordCount = Map({ passwordScoreDispatchedAt, count: 0 });
    const { root } = getRootComponent({ passwordScore, pwnedPasswordCount });
    const testConfirmPassword = 'Password1!x';
    root.find('TextField#password').find('input').simulate('change', { target: { value: password } });
    root.find('TextField#confirm-password').find('input').simulate('change', { target: { value: testConfirmPassword } })
      .simulate('blur');
    root.find('SetPasswordView').instance().lastPasswordScoreDispatchedAt = passwordScoreDispatchedAt;
    root.find('SetPasswordView').instance().forceUpdate();
    const updatedRoot = root.update();
    const updatedConfirmPasswordTextField = updatedRoot.find('TextField#confirm-password');
    expect(updatedConfirmPasswordTextField.prop('error')).toBe(true);
    expect(updatedConfirmPasswordTextField.prop('helperText')).toBe(errors.PASSWORDS_MUST_MATCH);
    updatedRoot.find('TextField#confirm-password').find('input').simulate('focus');
    expect(updatedRoot.find('TextField#confirm-password').prop('error')).toBe(false);
    expect(updatedRoot.find('Button').prop('disabled')).toBe(true);
  });

  it('does not validate that the password matches the confirm password when the password fails validation', () => {
    const passwordScoreDispatchedAt = new Date().getTime();
    const { root } = getRootComponent({ passwordScore: Map({ passwordScoreDispatchedAt, score: 0, feedback: {} }) });
    const testConfirmPassword = 'Password1!x';
    root.find('TextField#password').find('input').simulate('change', { target: { value: password } });
    root.find('TextField#confirm-password').find('input').simulate('change', { target: { value: testConfirmPassword } })
      .simulate('blur');
    root.find('SetPasswordView').instance().lastPasswordScoreDispatchedAt = passwordScoreDispatchedAt;
    root.find('SetPasswordView').instance().forceUpdate();
    const updatedRoot = root.update();
    const updatedConfirmPasswordTextField = updatedRoot.find('TextField#confirm-password');
    expect(updatedConfirmPasswordTextField.prop('error')).toBe(false);
    expect(updatedConfirmPasswordTextField.prop('helperText')).toBe('');
    expect(updatedRoot.find('Button').prop('disabled')).toBe(true);
  });

  it('accepts a validated password and dispatches the correct action when setting the password', () => {
    const passwordScoreDispatchedAt = new Date().getTime();
    const passwordScore = Map({ passwordScoreDispatchedAt, score: 4, feedback: {} });
    const pwnedPasswordCount = Map({ passwordScoreDispatchedAt, count: 0 });
    const { root, store } = getRootComponent({ passwordScore, pwnedPasswordCount });
    root.find('TextField#password').find('input').simulate('change', { target: { value: password } })
      .simulate('blur');
    root.find('TextField#confirm-password').find('input').simulate('change', { target: { value: password } })
      .simulate('blur');
    root.find('SetPasswordView').instance().lastPasswordScoreDispatchedAt = passwordScoreDispatchedAt;
    root.find('SetPasswordView').instance().forceUpdate();
    root.update().find('Button').simulate('submit');
    expect(store.getActions().pop()).toEqual({ type: SET_PASSWORD, payload: { userId, password } });
  });

  it('accepts a validated password and dispatches the correct action when resetting the password', () => {
    const passwordScoreDispatchedAt = new Date().getTime();
    const passwordScore = Map({ passwordScoreDispatchedAt, score: 4, feedback: {} });
    const pwnedPasswordCount = Map({ passwordScoreDispatchedAt, count: 0 });
    const { root, store } = getRootComponent({ activate: Map(), passwordScore, pwnedPasswordCount }, token);
    const testPassword = 'Password1!';
    root.find('TextField#password').find('input').simulate('change', { target: { value: testPassword } })
      .simulate('blur');
    root.find('TextField#confirm-password').find('input').simulate('change', { target: { value: testPassword } })
      .simulate('blur');
    root.find('SetPasswordView').instance().lastPasswordScoreDispatchedAt = passwordScoreDispatchedAt;
    root.find('SetPasswordView').instance().forceUpdate();
    root.update().find('Button').simulate('submit');
    expect(store.getActions().pop())
      .toEqual({ type: RESET_PASSWORD, payload: { actionId, email, password: testPassword } });
  });

  it('displays an error message when passed in from the store', () => {
    const error = 'Password failed server validation.';
    const { root } = getRootComponent({ setPassword: Map({ error }) });
    const passwordTextField = root.find('TextField#password');
    const passwordHelperTextEl = root.find('CardContent').find('Typography').at(1);
    expect(passwordTextField.prop('error')).toBe(true);
    expect(passwordHelperTextEl.text()).toBe(error);
  });

  it('allows the store error message to override the password validation error', () => {
    const error = 'Password failed server validation.';
    const passwordScoreDispatchedAt = new Date().getTime();
    const passwordScore = Map({ passwordScoreDispatchedAt, score: 0, feedback: {} });
    const { root } = getRootComponent({ passwordScore, setPassword: Map({ error }) });
    root.find('TextField#password').find('input').simulate('change', { target: { value: password } });
    root.find('TextField#confirm-password').find('input').simulate('change', { target: { value: `${password}1` } })
      .simulate('blur');
    root.find('SetPasswordView').instance().lastPasswordScoreDispatchedAt = passwordScoreDispatchedAt;
    root.find('SetPasswordView').instance().forceUpdate();
    const updatedRoot = root.update();
    const updatedConfirmPasswordTextField = updatedRoot.find('TextField#password');
    const updatedpasswordHelperTextEl = updatedRoot.find('CardContent').find('Typography').at(1);
    expect(updatedConfirmPasswordTextField.prop('error')).toBe(true);
    expect(updatedpasswordHelperTextEl.text()).toBe(error);
  });
});
