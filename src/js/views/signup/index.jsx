import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import ReCaptcha from 'react-google-recaptcha';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import handleChange from 'gnar-edge/es/handleChange';

import { signupActions } from 'actions';
import CardHeader from 'components/cardHeader';
import i18n from 'i18n';

const mapStateToProps = state => ({
  language: state.language.get('current')
});

const mapDispatchToProps = signupActions;

@connect(mapStateToProps, mapDispatchToProps)
export default class SignupView extends Component {
  static propTypes = {
    language: PropTypes.string.isRequired,
    submitSignupEmail: PropTypes.func.isRequired
  };

  state = {
    email: ''
  };

  handleChange = this::handleChange;

  executeCaptcha = e => {
    this.recaptchaInstance.execute();
    e.preventDefault();
  }

  /**
  * [Project Gnar]: This function isn't really necessary - you can remove it and inline your privacy statement.
  */
  privacyStatement = () => {
    /* istanbul ignore next */
    switch (window.location.hostname) {
      case 'app.gnar.ca':
        return (
          <Fragment>
            <Typography paragraph>
              This is the demo site of Project Gnar. It is a fully-functioning website. If you would like to sign
              up, your email address and any other information you choose to provide will be stored in our database.
            </Typography>
            <Typography>
              We value your privacy. We will not sell or abuse your information.
            </Typography>
          </Fragment>
        );
      case 'localhost':
        return (
          <Typography>
            TODO: Add a privacy statement here.
          </Typography>
        );
      default:
        return (
          <Fragment>
            <Typography paragraph>
              This application has been created from Project Gnar, an open source software project.
            </Typography>
            <Typography>
              The software developers who are working on this site haven&apos;t yet gotten around to replacing this
              message with their own privacy statement.
            </Typography>
          </Fragment>
        );
    }
  }

  submitSignupEmail = reCaptchaResponse => {
    this.setState({ loading: true });
    this.props.submitSignupEmail(this.state.email, reCaptchaResponse);
  };

  render() {
    const { language } = this.props;
    const { loading } = this.state;
    const {
      fieldLabels: { emailLabel },
      navButtons: { submitButtonText },
      signup: { cardTitle, content }
    } = i18n[language];
    return (
      <Grid container className='signup card-container' justify='center'>
        <Grid item lg={4} md={6} sm={8} xs={12}>
          <Card>
            <form onSubmit={this.executeCaptcha}>
              <CardHeader align='center' title={cardTitle} />
              <Divider />
              <CardContent>
                <TextField
                  autoFocus
                  fullWidth
                  disabled={loading}
                  id='email'
                  label={emailLabel}
                  margin='normal'
                  type='email'
                  onChange={this.handleChange('email')}
                />
                <Typography paragraph>
                  {content}
                </Typography>
                {this.privacyStatement()}
              </CardContent>
              <CardActions>
                <Grid container>
                  <Grid item align='left' xs={6}>
                    <Button
                      className='submit-button'
                      color='primary'
                      disabled={loading}
                      size='small'
                      type='submit'
                    >
                      {submitButtonText}
                    </Button>
                  </Grid>
                </Grid>
              </CardActions>
            </form>
            <ReCaptcha
              ref={el => { this.recaptchaInstance = el; }}
              sitekey={process.env.RECAPTCHA_SITE_KEY}
              size='invisible'
              onChange={this.submitSignupEmail}
            />
          </Card>
        </Grid>
      </Grid>
    );
  }
}
