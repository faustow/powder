import { Map } from 'immutable';
import { StaticRouter } from 'react-router';
import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';

import { ENGLISH } from 'i18n';
import { SUBMIT_SIGNUP_EMAIL } from 'actions/signup';
import { email, reCaptchaResponse } from 'mocks/constants';
import SignupView from 'views/signup';

const mockStore = configureStore();

const getRootComponent = () => {
  const store = mockStore({ language: Map({ current: ENGLISH }) });
  const root = mount((
    <StaticRouter context={{}}>
      <SignupView store={store} />
    </StaticRouter>
  ));
  return { root, store };
};

describe('<SignupView />', () => {
  it('renders properly', () => {
    const { root } = getRootComponent();
    const emailTextField = root.find('TextField#email');
    const emailLabel = emailTextField.find('label');
    const submitButton = root.find('Button');
    const reCAPTCHA = root.find('ReCAPTCHA');
    expect(emailTextField).toHaveLength(1);
    expect(emailLabel).toHaveLength(1);
    expect(emailLabel.text()).toBe('Email');
    expect(submitButton).toHaveLength(1);
    expect(submitButton.text()).toBe('Submit');
    expect(reCAPTCHA).toHaveLength(1);
    expect(reCAPTCHA.prop('onChange')).toBe(root.find('SignupView').instance().submitSignupEmail);
  });

  it('dispatches the correct action when an email has been entered and the Submit button is clicked', () => {
    const { root, store } = getRootComponent();
    root.find('#email').find('input').simulate('change', { target: { value: email } });
    root.update().find('Button[type="submit"]').simulate('submit');
    expect(store.getActions()).toEqual([{ type: SUBMIT_SIGNUP_EMAIL, payload: { email, reCaptchaResponse } }]);
  });

  it('dispatches the correct action when an email has been entered and the form is submitted', () => {
    const { root, store } = getRootComponent();
    root.find('#email').find('input').simulate('change', { target: { value: email } });
    root.update().find('form').simulate('submit');
    expect(store.getActions()).toEqual([{ type: SUBMIT_SIGNUP_EMAIL, payload: { email, reCaptchaResponse } }]);
  });
});
