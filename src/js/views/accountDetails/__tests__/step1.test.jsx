import React from 'react';
import { mount } from 'enzyme';
import Chance from 'chance';

import { ENGLISH, english } from 'i18n';
import { data, handleChange } from 'mocks/constants';
import AccountDetailsStep1 from 'views/accountDetails/step1';
import states, { CANADA, USA } from 'views/accountDetails/states';

const { accountDetails: { step1Greeting } } = english;

const chance = new Chance();

const getRootComponent = props => {
  const effectiveProps = {
    activation: false,
    ...data,
    handleChange,
    language: ENGLISH,
    ...props
  };
  return mount(<AccountDetailsStep1 {...effectiveProps} />);
};

describe('<AccountDetailsStep1 />', () => {
  it('renders properly', () => {
    const stateObj = states.find(({ value }) => value === data.state);
    const root = getRootComponent();
    const firstNameTextField = root.find({ autoComplete: 'given-name' }).find('TextField');
    const firstNameLabel = firstNameTextField.find('label');
    const firstNameInput = firstNameTextField.find('input');
    const lastNameTextField = root.find({ autoComplete: 'family-name' }).find('TextField');
    const lastNameLabel = lastNameTextField.find('label');
    const lastNameInput = lastNameTextField.find('input');
    const address1TextField = root.find({ autoComplete: 'address-line1' }).find('TextField');
    const address1Label = address1TextField.find('label');
    const address1Input = address1TextField.find('input');
    const address2TextField = root.find({ autoComplete: 'address-line2' }).find('TextField');
    const address2Label = address2TextField.find('label');
    const address2Input = address2TextField.find('input');
    const cityTextField = root.find({ autoComplete: 'address-level2' }).find('TextField');
    const cityLabel = cityTextField.find('label');
    const cityInput = cityTextField.find('input');
    const stateTextField = root.find({ autoComplete: 'address-level1' }).find('TextField');
    const stateLabel = stateTextField.find('label');
    const stateInput = stateTextField.find('input');
    const stateFormControl = root.find('.state-select').find('FormControl');
    const stateFormControlLabel = stateFormControl.find('label');
    const stateFormControlSelect = stateFormControl.find('Select');
    const postalCodeTextField = root.find({ autoComplete: 'postal-code' }).find('TextField');
    const postalCodeLabel = postalCodeTextField.find('label');
    const postalCodeInput = postalCodeTextField.find('input');
    expect(root.find("Typography[variant='headline']")).toHaveLength(0);
    expect(firstNameTextField).toHaveLength(1);
    expect(firstNameInput).toHaveLength(1);
    expect(firstNameInput.prop('value')).toBe(data.firstName);
    expect(firstNameLabel).toHaveLength(1);
    expect(firstNameLabel.text()).toBe('First Name');
    expect(lastNameTextField).toHaveLength(1);
    expect(lastNameInput).toHaveLength(1);
    expect(lastNameInput.prop('value')).toBe(data.lastName);
    expect(lastNameLabel).toHaveLength(1);
    expect(lastNameLabel.text()).toBe('Last Name');
    expect(address1TextField).toHaveLength(1);
    expect(address1Input).toHaveLength(1);
    expect(address1Input.prop('value')).toBe(data.address1);
    expect(address1Label).toHaveLength(1);
    expect(address1Label.text()).toBe('Street Address');
    expect(address2TextField).toHaveLength(1);
    expect(address2Input).toHaveLength(1);
    expect(address2Input.prop('value')).toBe(data.address2);
    expect(address2Label).toHaveLength(1);
    expect(address2Label.text()).toBe('Apt., ste., bldg.');
    expect(cityTextField).toHaveLength(1);
    expect(cityInput).toHaveLength(1);
    expect(cityInput.prop('value')).toBe(data.city);
    expect(cityLabel).toHaveLength(1);
    expect(cityLabel.text()).toBe('City');
    expect(stateTextField).toHaveLength(1);
    expect(stateInput).toHaveLength(1);
    expect(stateInput.prop('value')).toBe(undefined);
    expect(stateLabel).toHaveLength(1);
    expect(stateLabel.text()).toBe(' ');
    expect(stateFormControl).toHaveLength(1);
    expect(stateFormControlSelect).toHaveLength(1);
    expect(stateFormControlSelect.prop('value')).toBe(data.state);
    expect(stateFormControlLabel).toHaveLength(1);
    expect(stateFormControlLabel.text()).toBe(stateObj.country === CANADA ? 'Province' : 'State');
    expect(postalCodeTextField).toHaveLength(1);
    expect(postalCodeInput).toHaveLength(1);
    expect(postalCodeInput.prop('value')).toBe(data.postalCode);
    expect(postalCodeLabel).toHaveLength(1);
    expect(postalCodeLabel.text()).toBe(stateObj.country === CANADA ? 'Postal Code' : 'ZIP Code');
  });

  it('displays a special greeting on activation', () => {
    const root = getRootComponent({ activation: true });
    expect(root.find("Typography[variant='headline']").text()).toBe(step1Greeting);
  });

  it('updates the parent component with any field change', () => {
    [CANADA, USA].forEach(country => {
      const root = getRootComponent();
      const updated = {
        firstName: chance.first(),
        lastName: chance.last(),
        address1: chance.address(),
        address2: chance.bool() ? '' : `Apt ${chance.integer({ max: 1000, min: 1 })}`,
        city: chance.city(),
        state: country === CANADA ? chance.province() : chance.state(),
        stateFull: country === CANADA ? chance.province({ full: true }) : chance.state({ full: true }),
        postalCode: country === CANADA ? chance.postal() : chance.zip()
      };
      const firstNameChange = { target: { value: updated.firstName } };
      root.find({ autoComplete: 'given-name' }).find('input').simulate('change', firstNameChange);
      expect(handleChange('firstName')).toHaveReturnedWith(updated.firstName);
      const lastNameChange = { target: { value: updated.lastName } };
      root.find({ autoComplete: 'family-name' }).find('input').simulate('change', lastNameChange);
      expect(handleChange('lastName')).toHaveReturnedWith(updated.lastName);
      const address1Change = { target: { value: updated.address1 } };
      root.find({ autoComplete: 'address-line1' }).find('input').simulate('change', address1Change);
      expect(handleChange('address1')).toHaveReturnedWith(updated.address1);
      const address2Change = { target: { value: updated.address2 } };
      root.find({ autoComplete: 'address-line2' }).find('input').simulate('change', address2Change);
      expect(handleChange('address2')).toHaveReturnedWith(updated.address2);
      const cityChange = { target: { value: updated.city } };
      root.find({ autoComplete: 'address-level2' }).find('input').simulate('change', cityChange);
      expect(handleChange('city')).toHaveReturnedWith(updated.city);
      const stateChange1 = { target: { value: updated.state } };
      root.find({ autoComplete: 'address-level1' }).find('input').simulate('change', stateChange1);
      expect(handleChange('state')).toHaveReturnedWith(updated.state);
      const stateChange2 = { target: { value: updated.stateFull } };
      root.find({ autoComplete: 'address-level1' }).find('input').simulate('change', stateChange2);
      expect(handleChange('state')).toHaveReturnedWith(updated.state);
      const stateChange3 = { target: { value: 'invalid' } };
      root.find({ autoComplete: 'address-level1' }).find('input').simulate('change', stateChange3);
      expect(handleChange('state')).toHaveReturnedWith('');
      const selectState = chance.pickone(states);
      root.find("Select [role='button']").simulate('click');
      root.find(`Select [data-value='${selectState.value}']`).first().simulate('click');
      expect(handleChange('state')).toHaveReturnedWith(selectState.value);
      const postalCodeChange = { target: { value: updated.postalCode } };
      root.find({ autoComplete: 'postal-code' }).find('input').simulate('change', postalCodeChange);
      expect(handleChange('postalCode')).toHaveReturnedWith(updated.postalCode);
    });
  });

  it('updates all fields when the parent component passes in new props', () => {
    [CANADA, USA].forEach(country => {
      const root = getRootComponent();
      const updated = {
        firstName: chance.first(),
        lastName: chance.last(),
        address1: chance.address(),
        address2: chance.bool() ? '' : `Apt ${chance.integer({ max: 1000, min: 1 })}`,
        city: chance.city(),
        state: country === CANADA ? chance.province() : chance.state(),
        postalCode: country === CANADA ? chance.postal() : chance.zip()
      };
      root.setProps(updated);
      expect(root.find({ autoComplete: 'given-name' }).find('input').prop('value')).toBe(updated.firstName);
      expect(root.find({ autoComplete: 'family-name' }).find('input').prop('value')).toBe(updated.lastName);
      expect(root.find({ autoComplete: 'address-line1' }).find('input').prop('value')).toBe(updated.address1);
      expect(root.find({ autoComplete: 'address-line2' }).find('input').prop('value')).toBe(updated.address2);
      expect(root.find({ autoComplete: 'address-level2' }).find('input').prop('value')).toBe(updated.city);
      expect(root.find('.state-select').find('Select').prop('value')).toBe(updated.state);
      expect(root.find({ autoComplete: 'postal-code' }).find('input').prop('value')).toBe(updated.postalCode);
    });
  });
});
