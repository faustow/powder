/* eslint-disable no-proto */
export default class localStorageMock {
  constructor() {
    this.overrides.forEach(prop => { localStorage.__proto__[prop] = jest.fn(this[prop]); });
  }

  deactivate() {
    this.overrides.forEach(prop => { localStorage.__proto__[prop] = this.originals[prop]; });
  }

  items = {};

  overrides = ['clear', 'getItem', 'removeItem', 'setItem'];

  originals = this.overrides.reduce((memo, prop) => {
    memo[prop] = localStorage.__proto__[prop];
    return memo;
  }, {});

  clear = () => {
    this.items = {};
  }

  getItem = key => this.items[key] || null;

  setItem = (key, value) => {
    this.items[key] = value;
  }

  removeItem = key => {
    delete this.items[key];
  }
}
