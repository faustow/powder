{
  "env": {
    "browser": true,
    "es6": true,
    "jasmine": true,
    "jest": true
  },
  "extends": "airbnb",
  "globals": {
    "_": false
  },
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true,
      "legacyDecorators": true
    }
  },
  "plugins": [
    "react"
  ],
  "rules": {
    "arrow-parens": [ 2, "as-needed" ],
    "block-scoped-var": 0,
    "comma-dangle": [ 2, "never" ],
    "curly": [ 2, "all" ],
    "default-case": 0,
    "func-names": 0,
    "func-style": [ 2, "declaration", { "allowArrowFunctions": true } ],
    "implicit-arrow-linebreak": 0,
    "import/no-extraneous-dependencies": [ 2, {
        "devDependencies": [
          "jest.setup.js",
          "src/__mocks__/*",
          "**/*.test.js{x,}",
          "**/webpack.config.babel.js"
        ],
        "optionalDependencies": false
      } ],
    "import/no-unresolved": 0,
    "import/order": [0, {
        "groups": [ [ "builtin", "external" ], [ "index", "internal", "parent", "sibling" ] ],
        "newlines-between": "always"
      } ],
    "indent": [ 2, 2, { "SwitchCase": 1 } ],
    "jsx-a11y/anchor-is-valid": [ 2, {
        "components": [ "Link" ],
        "specialLink": [ "to", "hrefLeft", "hrefRight" ],
        "aspects": [ "noHref", "invalidHref", "preferButton" ]
      } ],
    "jsx-quotes": [ 2, "prefer-single" ],
    "max-len": [ 2, 120, 2, { "ignoreUrls": true, "ignoreComments": true } ],
    "newline-per-chained-call": [ 2, { ignoreChainWithDepth: 6 } ],
    "no-alert": 2,
    "no-bitwise": 0,
    "no-console": [ 2, { "allow": [ "error", "info", "warn" ] } ],
    "no-constant-condition": 2,
    "no-else-return": 0,
    "no-extra-parens": 2,
    "no-floating-decimal": 0,
    "no-mixed-operators": 0,
    "no-multi-spaces": [ 2, { "ignoreEOLComments": true }],
    "no-nested-ternary": 0,
    "no-param-reassign": [ 2, { "props": false } ],
    "no-plusplus": 0,
    "no-process-exit": 2,
    "no-unused-expressions": [ 2, { "allowShortCircuit": true } ],
    "object-curly-newline": [ 2, {
        "ObjectExpression": { "minProperties": 8, "multiline": true, "consistent": true },
        "ObjectPattern": { "minProperties": 8, "multiline": true, "consistent": true },
        "ImportDeclaration": { "minProperties": 8, "multiline": true, "consistent": true },
        "ExportDeclaration": { "minProperties": 8, "multiline": true, "consistent": true }
      } ],
    "operator-linebreak": [ 2, "after", { "overrides": { "?": "before", ":": "before" } } ],
    "react/destructuring-assignment": 0,
    "react/jsx-curly-brace-presence": [ 2, "never" ],
    "react/jsx-first-prop-new-line": [ 2, "multiline" ],
    "react/jsx-one-expression-per-line": 0,
    "react/jsx-key": 2,
    "react/jsx-sort-default-props": 2,
    "react/jsx-sort-props": [ 2, {
        "callbacksLast": true,
        "shorthandFirst": true,
        "ignoreCase": true,
        "reservedFirst": true
      } ],
    "react/sort-prop-types": 2
  },
  "settings": {
    "import/resolver": {
      "babel-module": {}
    }
  }
}
