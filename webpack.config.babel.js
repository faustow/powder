import path from 'path';
import _ from 'lodash';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import { EnvironmentPlugin, IgnorePlugin, ProvidePlugin } from 'webpack';
import FaviconsWebpackPlugin from 'favicons-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin';
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';
import autoprefixer from 'autoprefixer';

/*
 * [Project Gnar]: Jest does not use webpack, its config is specified in the "jest" portion of package.json
 *            ref: facebook.github.io/jest/docs/en/configuration.html
 */

export default (env = {}, argv) => {
  const { analyze, open, production } = env;
  const isProd = production;
  const mode = isProd ? 'production' : 'development';
  const startServer = /dev-server$/.test(argv.$0);
  const profile = analyze || !isProd && !startServer;

  const origins = {
    i18n: 'http://localhost:9400',
    user: 'http://localhost:9400'
  };

  const devServer = startServer ? {
    devServer: {
      compress: true,
      contentBase: 'dist/assets',
      historyApiFallback: true,
      host: 'localhost',
      open,
      port: 3000,
      quiet: false
    }
  } : {};

  const globals = {
    _: 'lodash'
  };

  const indexHtmlConfig = {
    title: 'Gnar',
    template: './src/assets/template/index.htm',
    filename: 'index.html',
    inject: 'body'
  };

  const fontRule = {
    test: /\.(eot|svg|ttf|woff2?)(\?.+)?$/,
    loader: 'url-loader',  // defaults to file-loader (with options) when image size > options.limit
    options: {
      name: 'assets/fonts/[name].[ext]',
      limit: 8192
    }
  };

  const imageRule = {
    test: /\.(ico|gif|jpe?g|png)(\?.+)?$/i,
    use: [
      {
        loader: 'url-loader',  // defaults to file-loader (with options) when image size > options.limit
        options: {
          limit: 8192,
          name: 'assets/[sha512:hash].[ext]'
        }
      },
      'image-webpack-loader'
    ]
  };

  const jsRule = {
    test: /\.jsx?$/,
    exclude: /node_modules\/(?!gnar-edge)/,
    use: 'babel-loader'
  };

  const scssLoaders = [
    'style',
    'css',
    {
      loader: 'postcss-loader',
      options: {
        plugins: () => [autoprefixer]
      }
    },
    'resolve-url',
    {
      loader: 'sass-loader',
      options: {
        includePaths: [
          './src'
        ]
      }
    },
    {
      loader: 'sass-resources-loader',
      options: {
        resources: './src/css/*'
      }
    }
  ].map(loader => _.set(_.isObject(loader) ? loader : { loader: `${loader}-loader` }, 'options.sourceMap', !isProd));

  isProd && (scssLoaders[0] = MiniCssExtractPlugin.loader);
  const scssRule = {
    test: /\.s?css$/,
    use: scssLoaders
  };

  const txtRule = {
    test: /\.(md|txt)$/,
    use: 'raw-loader'
  };

  const reCaptchaSiteKeyEnv = `${env.envPrefix || process.env.GNAR_POWDER_ENV_PREFIX || 'GNAR'}_RECAPTCHA_SITE_KEY`;
  const reCaptchaSiteKey = process.env[reCaptchaSiteKeyEnv];

  if (!reCaptchaSiteKey) {
    console.warn(
      '\x1b[31m\x1b[1m',
      `>>>>> ${reCaptchaSiteKeyEnv} not found. Add this environment variable to enable reCAPTCHA. <<<<<\n`,
      '\x1b[0m'
    );
  }

  const plugins = [
    new EnvironmentPlugin({
      NODE_ENV: mode,
      ORIGINS: JSON.stringify(startServer ? origins : {}),
      RECAPTCHA_SITE_KEY: reCaptchaSiteKey  // Inline the site key if you don't want to fetch it from the env
    }),
    new FaviconsWebpackPlugin({ logo: './src/assets/images/favicon.png', prefix: 'assets/icons/[hash]/' }),
    new IgnorePlugin(/^\.\/locale$/, /moment$/),  // Remove (or narrow) this if you need moment i18n support.
    new ProvidePlugin(globals),
    new HtmlWebpackPlugin(indexHtmlConfig)
  ];

  isProd && plugins.push(new MiniCssExtractPlugin({
    filename: '[name].[hash].css'
  }));

  profile && plugins.push(new BundleAnalyzerPlugin());

  const cacheGroup = (memo, dep, idx, deps) => {
    const name = dep.replace(/@/, '');
    memo[name] = {
      test: RegExp(`[\\/]node_modules[\\/]${dep === 'vendor' ? `(?!(${deps.join('|')}))` : dep}`),
      name,
      chunks: 'initial',
      minSize: 0,
      minChunks: 1,
      enforce: true
    };
  };

  return {
    ...devServer,
    devtool: isProd ? 'source-map' : 'inline-source-map',  // set UglifyJsPlugin.sourceMap true for source maps in prod
    entry: {
      main: './src/js/router'
    },
    mode,
    module: {
      rules: [fontRule, imageRule, jsRule, scssRule, txtRule]
    },
    optimization: {
      minimizer: [
        new OptimizeCSSAssetsPlugin({}),
        new UglifyJsPlugin({
          cache: true,
          parallel: true,
          sourceMap: !isProd
        })
      ],
      splitChunks: {
        chunks: 'all',
        cacheGroups: _.transform(['immutable', 'lodash', '@material-ui', 'moment', 'react', 'vendor'], cacheGroup, {})
      }
    },
    output: {
      path: path.resolve('dist'),
      filename: '[name].[hash].bundle.js',
      chunkFilename: '[name].[chunkhash].bundle.js',
      publicPath: '/'
    },
    performance: {
      maxAssetSize: 512000,
      maxEntrypointSize: 1048576
    },
    plugins,
    profile,
    resolve: {
      extensions: ['.js', '.jsx']
    }
  };
};
